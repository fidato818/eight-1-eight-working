'use client'
import React from 'react'

import Link from 'next/link'
import Image from 'next/image'

import { useEffect } from 'react'
import { useSetState } from 'ahooks'

const Footer = () => {
    const [state, setState] = useSetState({
        backToTop: false, 
        arrLinks: [
            {
                mainClass: 'whatsapp',
                iconClass: 'bi bi-whatsapp',
                iconLink: 'https://wa.me/923453048496',
            },
            {
                mainClass: 'facebook',
                iconClass: 'bi bi-facebook',
                iconLink: 'https://facebook.com/millions818/',
            },
            {
                mainClass: 'github',
                iconClass: 'bi bi-github',
                iconLink: 'https://github.com/fidato818',
            },
            {
                mainClass: 'youtube',
                iconClass: 'bi bi-youtube',
                iconLink: 'https://youtube.com/@adnanahmed4662',
            },
            {
                mainClass: 'instagram',
                iconClass: 'bi bi-instagram',
                iconLink: 'https://instagram.com/adnan_ahmed818/',
            },
            {
                mainClass: 'twitter',
                iconClass: 'bi bi-twitter-x',
                iconLink: 'https://twitter.com/AdnanAhmed_818',
            },
            {
                mainClass: 'linkedin',
                iconClass: 'bi bi-linkedin',
                iconLink: 'https://linkedin.com/in/adnanahmedofficial/',
            },
        ],
    })

    useEffect(() => {
        let backtotop: HTMLElement | null = document.getElementById('back-to')
        window.addEventListener('scroll', (event) => {
            if (window.scrollY > 100) {
                backtotop?.classList.add('active')
            } else {
                backtotop?.classList.remove('active')
            }
        })
    }, [])

    return (
        <>
            <footer id="footer">
                <div className="footer-top">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-3 col-md-6 footer-contact">
                                {/* <Image src={require('@/assets/images/app-logo-preview.png')} alt="" width={100} height={100} /> */}
                                <h3>818 | Digital Agency</h3>
                                <p>
                                    818 | Digital Agency is a versatile entity
                                    specializing in leveraging digital
                                    technologies to drive online success.
                                    {/* hey combine strategy, web development, mobile app development, and data analysis to help clients thrive in the ever-evolving online world. */}
                                    <br />
                                    <br />
                                    <strong>Phone:</strong> +92 345 304 8496
                                    <br />
                                    <strong>Email:</strong>{' '}
                                    adnanahmed818@gmail.com
                                    <br />
                                </p>
                            </div>
                            <div className="col-lg-2 col-md-6 footer-links">
                                <h4>Our Company</h4>
                                <ul>
                                    <li>
                                        <i className="bx bx-chevron-right"></i>{' '}
                                        <Link href="/">Home</Link>
                                    </li>
                                    <li>
                                        <i className="bx bx-chevron-right"></i>{' '}
                                        <Link href="/about">About us</Link>
                                    </li>
                                    <li>
                                        <i className="bx bx-chevron-right"></i>{' '}
                                        <Link href="/services">Services</Link>
                                    </li>
                                    <li>
                                        <i className="bx bx-chevron-right"></i>{' '}
                                        <Link href="/portfolio">Portfolio</Link>
                                    </li>
                                    <li>
                                        <i className="bx bx-chevron-right"></i>{' '}
                                        <Link href="/contact">Contact us</Link>
                                    </li>
                                </ul>
                            </div>
                            <div className="col-lg-3 col-md-6 footer-links">
                                <h4>Our Services</h4>
                                <ul>
                                    {/* <li><i className="bx bx-chevron-right"></i> <Link href="#">Web Design</Link></li> */}
                                    <li>
                                        <i className="bx bx-chevron-right"></i>{' '}
                                        <Link href="/pages/web-development">
                                            Web Development
                                        </Link>
                                    </li>
                                    <li>
                                        <i className="bx bx-chevron-right"></i>{' '}
                                        <Link href="/pages/point-of-sale">
                                            Point of Sale
                                        </Link>
                                    </li>
                                    <li>
                                        <i className="bx bx-chevron-right"></i>{' '}
                                        <Link href="/pages/e-commerce">
                                            E Commerce
                                        </Link>
                                    </li>
                                    <li>
                                        <i className="bx bx-chevron-right"></i>{' '}
                                        <Link href="/pages/mobile-app-development">
                                            Mobile App Development
                                        </Link>
                                    </li>
                                </ul>
                            </div>

                            {/* <div className="col-lg-4 col-md-6 footer-newsletter">
                                <h4>Join Our Newsletter</h4>
                                <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
                                <form action="" method="post">
                                    <input type="email" name="email" /><input type="submit" value="Subscribe" />
                                </form>
                            </div> */}
                            <div className="col-lg-4 col-md-6 footer-newsletter">
                                <h4>
                                    Are you interested in collaborating with us?
                                    Click Below to Get Started.
                                </h4>
                                <div className="  pt-3 pt-md-0 ">
                                    <Link href="/project-planner" className="">
                                        <Image
                                            src={require('../assets/images/-preview-3.png')}
                                            alt=""
                                            className="img-fluid"
                                            width={200}
                                            height={200}
                                        />
                                    </Link>
                                </div>
                                <h4 className="mt-3">Follow us</h4>
                                <div className="social-links ">
                                    {state.arrLinks.map((e, i: number) => {
                                        return (
                                            <Link
                                            target="_blank"
                                                key={i}
                                                href={e.iconLink}
                                                className={e.mainClass}
                                            >
                                                <i className={e.iconClass}></i>
                                            </Link>
                                        )
                                    })}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container  py-4">
                    {/* <div className="me-md-auto text-center text-md-start"> */}
                    <div className="text-center">
                        <div
                            className="copyright"
                            style={{ textTransform: 'uppercase' }}
                        >
                            {/* &copy; @ {new Date().getFullYear()} by 818 | Digital Agency */}
                            {/* <strong></strong>. All Rights Reserved */}
                            Copyright &copy; {new Date().getFullYear()}. 818 |
                            Digital Agency. All rights reserved
                        </div>
                        {/* <div className="credits">

                            Designed by <a href="bootstrapmade.com/">BootstrapMade</a>
                        </div> */}
                    </div>
                </div>
            </footer>

            <button
                onClick={() =>
                    window.scrollTo({
                        top: 0,
                        behavior: 'smooth',
                    })
                }
                id="back-to"
                className="back-to-top d-flex align-items-center justify-content-center"
            >
                <i className="bi bi-arrow-up-short"></i>
            </button>
        </>
    )
}
export default Footer
