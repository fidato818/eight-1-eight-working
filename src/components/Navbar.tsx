'use client'
import React from 'react'
import { useAppSelector } from '@/app/store/hooks'
import { usePathname } from 'next/navigation'
import Link from 'next/link'
import Image from 'next/image'

import { useEffect } from 'react'
import { useSetState } from 'ahooks'

const Navbar = () => {
    const userFromRedux = useAppSelector(
        (state) => state.userReducer.user || ''
    )
    const { email }: any = userFromRedux
    const [state, setState] = useSetState({
        backToTop: false,
        useMobleNav: false,
        arrLinks: [
            // { linkName: 'Dashboard', linkAddress: '/dashboard' },
            { linkName: 'Home', linkAddress: '/' },
            { linkName: 'About', linkAddress: '/about' },
            { linkName: 'Services', linkAddress: '/services' },
            { linkName: 'Testimonials', linkAddress: '/testimonials' },
            { linkName: 'Blogs', linkAddress: '/blogs' },
            { linkName: 'Portfolio', linkAddress: '/portfolio' },
            { linkName: 'Contact', linkAddress: '/contact' },
            // { linkName: 'Project Planner', linkAddress: '/contact' },
        ],
    })
    const pathname = usePathname()

    // const []
    useEffect(() => {
        let selectHeader: HTMLElement | null = document.getElementById(
            'header'
        ) as HTMLInputElement
        const headerOffset = selectHeader.offsetTop
        let nextElement = selectHeader?.nextElementSibling
        window.addEventListener('scroll', (event) => {
            if (headerOffset - window.scrollY <= 0) {
                selectHeader?.classList.add('fixed-top')
                selectHeader?.classList.add('shadow')
                nextElement?.classList.add('scrolled-offset')
            } else {
                selectHeader?.classList.remove('fixed-top')
                nextElement?.classList.remove('scrolled-offset')
            }
        })
    }, [])
    useEffect(() => {
        let backtotop: HTMLElement | null = document.getElementById('back-to')
        window.addEventListener('scroll', (event) => {
            if (window.scrollY > 100) {
                backtotop?.classList.add('active')
            } else {
                backtotop?.classList.remove('active')
            }
        })
    }, [])

    const forMobile = () => {
        // const buttonData: HTMLElement | null = document.getElementById('navbar')
        // const respIc: HTMLElement | null = document.getElementById('respIcon')
        // buttonData?.classList.toggle('navbar-mobile')
        // respIc?.classList.add('bi-x')
        // respIc?.classList.remove('bi-list')
        setState({ useMobleNav: !state.useMobleNav })
        console.log('data')
    }

    const splitPath = pathname.split('/')
    const hasUpperCase = (str: string) => /[A-Z]/.test(str)

    const navFilter = state.arrLinks.filter((e) =>
        email !== undefined ? e.linkAddress : e.linkAddress !== '/dashboard'
    )
    const { useMobleNav } = state
    return (
        <>
            <section id="topbar" className="d-flex align-items-center ">
                <div className="container d-flex justify-content-center justify-content-md-between">
                    <div className="contact-info d-flex align-items-center">
                        <i className="bi bi-envelope d-flex align-items-center">
                            <a href="mailto:adnanahmed818@gmail.com">
                                adnanahmed818@gmail.com
                            </a>
                        </i>
                        <i className="bi bi-phone d-flex align-items-center ms-4">
                            <span>+92 345 304 8496</span>
                        </i>
                    </div>
                    <div className="social-links d-none d-md-flex align-items-center">
                        <Link
                            target="_blank"
                            href="https://twitter.com/AdnanAhmed_818"
                            className="twitter"
                        >
                            <i className="bi bi bi-twitter-x"></i>
                        </Link>
                        <Link
                            target="_blank"
                            href="https://facebook.com/millions818/"
                            className="facebook"
                        >
                            <i className="bi bi-facebook"></i>
                        </Link>
                        <Link
                            target="_blank"
                            href="https://instagram.com/adnan_ahmed818/"
                            className="instagram"
                        >
                            <i className="bi bi-instagram"></i>
                        </Link>
                        <Link
                            target="_blank"
                            href="https://linkedin.com/in/adnanahmedofficial/"
                            className="linkedin"
                        >
                            <i className="bi bi-linkedin"></i>
                        </Link>
                    </div>
                </div>
            </section>
            <header id="header" className="d-flex align-items-center shadow-md">
                <div className="container">
                    <div className="d-flex justify-content-between align-items-center">
                        <div className="logo">
                            <h1 className="text-light">
                                <Link href="/">
                                    {/* <span
                                        style={{
                                            textTransform: 'lowercase',
                                            fontSize: '0.8em',
                                        }}
                                    >
                                       818 | Digital Age
                                    </span> */}
                                    {/* <br /> */}
                                    <span
                                        style={{
                                            textTransform: 'capitalize',
                                            fontSize: '0.8em',
                                        }}
                                    >
                                        818 | Digital Agency
                                    </span>
                                </Link>
                                {/* <Image
                                    src={require('../assets/images/123-removebg-preview.png')}
                                    alt=""
                                    width="100"
                                    height="100"
                                /> */}
                            </h1>
                            {/* <!-- Uncomment below if you prefer to use an image logo --> */}
                            {/* <!-- <Link href="index.html"><Image src="assets/img/logo.png" alt="" className="img-fluid"></Link>--> */}
                        </div>

                        <nav id="navbar" className="navbar">
                            <ul>
                                {/* {email !== undefined && (
                                <li>
                                    <Link
                                        className={
                                            '/dashboard' === pathname
                                                ? 'active'
                                                : ''
                                        }
                                        href="/dashboard"
                                    >
                                        Dashboard
                                    </Link>
                                </li>
                            )} */}
                                {/* {navFilter.map((e, i) => { */}
                                {state.arrLinks.map((e, i) => {
                                    return (
                                        <li key={i}>
                                            <Link
                                                className={
                                                    e.linkAddress === pathname
                                                        ? 'active'
                                                        : ''
                                                }
                                                href={e.linkAddress}
                                            >
                                                {e.linkName}
                                            </Link>
                                        </li>
                                    )
                                })}
                            </ul>

                            <span onClick={() => forMobile()}>
                                <i className="bi mobile-nav-toggle bi-list "></i>
                            </span>
                        </nav>
                    </div>
                    {/* <!-- .navbar --> */}
                </div>
                {useMobleNav && (
                    <nav
                        id="navbar"
                        className="navbar-mobile"
                        style={{ overflow: 'hidden' }}
                    >
                        <ul
                            style={{
                                display: 'block',
                                position: 'absolute',
                                // top: '55px',
                                right: '15px',
                                bottom: '15px',
                                left: '15px',
                                padding: '10px 0',
                                backgroundColor: '#fff',
                                overflowY: 'auto',
                                transition: '0.3s',
                            }}
                        >
                            {state.arrLinks.map((e, i) => {
                                return (
                                    <li
                                        key={i}
                                        style={{
                                            position: 'relative',
                                            margin: '18px 0 ',
                                            textTransform: 'uppercase',
                                        }}
                                    >
                                        <Link
                                            className={
                                                e.linkAddress === pathname
                                                    ? 'active'
                                                    : ''
                                            }
                                            href={e.linkAddress}
                                        >
                                            <span
                                                onClick={() =>
                                                    setTimeout(() => {
                                                        forMobile()
                                                    }, 1000)
                                                }
                                            >
                                                {e.linkName}
                                            </span>
                                        </Link>
                                    </li>
                                )
                            })}
                        </ul>

                        <span onClick={() => forMobile()}>
                            <i className="bi mobile-nav-toggle bi-x"></i>
                        </span>
                    </nav>
                )}
            </header>

            {pathname !== '/' && (
                <section id="breadcrumbs" className="breadcrumbs">
                    <div className="container">
                        <div className="d-flex justify-content-between align-items-center">
                            <>
                                <ol>
                                    <li>
                                        <Link href="/">Home</Link>
                                    </li>
                                    <li style={{ textTransform: 'capitalize' }}>
                                        {splitPath[1] &&
                                            splitPath[1].replaceAll('-', ' ')}
                                    </li>
                                    {hasUpperCase(splitPath[2]) === true ? (
                                        <li
                                            style={{
                                                textTransform: 'capitalize',
                                            }}
                                        >
                                            Portfolio Detail
                                        </li>
                                    ) : (
                                        <li
                                            style={{
                                                textTransform: 'capitalize',
                                            }}
                                        >
                                            {splitPath[2] &&
                                                splitPath[2].replaceAll(
                                                    '-',
                                                    ' '
                                                )}
                                        </li>
                                    )}
                                </ol>
                            </>
                        </div>
                    </div>
                </section>
            )}
        </>
    )
}
export default Navbar
