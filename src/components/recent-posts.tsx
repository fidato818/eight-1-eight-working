'use client'
import React, { useState, useEffect, useLayoutEffect } from 'react'
import {
    ref as ref_database,
    remove,
    onValue,
    child,
    push,
    set,
    update,
} from 'firebase/database'
import Image from 'next/image'
import moment from 'moment'
import { auth, db } from '@/config/firebaseConfig'
import { useSetState } from 'ahooks'
const RecentPosts = () => {
    const [state, setState] = useSetState<any | null>({ arr: [] })
    const [isClient, setIsClient] = useState(false)
    useEffect(() => {
        setIsClient(true)
    }, [])
    useLayoutEffect(() => {
        setState({ spinnerLoader: true })
        const getData = () => {
            const allPostData = ref_database(db, 'blogsData')
            onValue(allPostData, (snapshot) => {
                var newArr: any[] = []
                const data = snapshot.val()
                snapshot.forEach((data) => {
                    var childData = data.val()
                    newArr.push(childData)
                })

                setState({ arr: newArr.reverse(), spinnerLoader: false })
            })
        }
        getData()
    }, [setState])
    return (
        <div>
            <>
                {state.arr.map((e: any, i: number) => {
                    return (
                        <>
                            <div className="post-item clearfix">
                                <Image
                                    src={e.downloadURL}
                                    alt=""
                                    width={100}
                                    height={100}
                                />
                                <h4>
                                    <a href="blog-single.html">
                                        {e.headingName}
                                    </a>
                                </h4>
                                <time dateTime="2020-01-01">
                                    {moment(e.createAt).format('MMM D, YYYY')}
                                </time>
                            </div>
                        </>
                    )
                })}
            </>
        </div>
    )
}

export default RecentPosts
