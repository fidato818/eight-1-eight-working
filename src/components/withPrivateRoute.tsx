import { useRouter } from 'next/navigation'
import React, { useEffect } from 'react'
import { useAppSelector } from '../app/store/hooks'

const ProtectedRoute = ({ children }: { children: React.ReactNode }) => {
    const userFromRedux = useAppSelector(
        (state) => state.userReducer.user || ''
    )
    // console.log('userFromRedux', userFromRedux)
    const router = useRouter()

    useEffect(() => {
        if (userFromRedux == '') {
            router.replace('/login')
        }
    }, [router, userFromRedux])

    return <>{userFromRedux ? children : null}</>
}

export default ProtectedRoute
