'use client'
import React from 'react'
import Image from 'next/image'
import Navbar from '@/components/Navbar'
import Footer from '@/components/Footer'
import { Carousel } from 'react-bootstrap'

import CountUp, { useCountUp } from 'react-countup'

//Components
import ClientComponent from '@/components/client-section'
import ServiceComponent from '@/components/service-section'

import { useSetState } from 'ahooks'
import {
    query,
    orderByChild,
    ref,
    onValue,
    equalTo,
    limitToLast,
} from 'firebase/database'

import Link from 'next/link'

import { db } from '@/config/firebaseConfig'
import { useEffect } from 'react'
interface sample_arr {
    indexCarousel: number
    arrayCarousel: { image: string; caption: string; description: string }[]
    arrPortfolio: {
        body: string
        buttonLink: string
        category: string
        createAt: string
        currentUser: string
        downloadURL: string
        githubLink: string
        headingName: string
        newPostKey: string
        status: boolean
        updateAt: string
    }[]
}

const Page = () => {
    const [state, setState] = useSetState<sample_arr>({
        arrayCarousel: [
            {
                image: require('@/assets/images/OFFICE-IT.jpg'),
                caption: `Let's Build Better Solutions For Your Business`,
                description:
                    "We are a proficient team of designers and developers specializing in creating outstanding websites, web applications, and mobile apps, tailored to your unique requirements and preferences. Let's bring your digital ideas to life!",
            },
            {
                image: require('@/assets/images/css-coding.jpg'),
                caption: `Your New Online Presence with 818 | Digital Agency `,
                description:
                    "We're a talented team of designers and developers passionate about creating outstanding websites, web applications, and mobile apps. Our expertise is your gateway to exceptional digital solutions.",
            },
            {
                image: require('@/assets/images/multiple-desktop.jpg'),
                caption: `Let's Connect`,
                description:
                    "Let's connect to explore how our expertise in design and development can transform your digital presence. Together, we'll craft innovative solutions for websites, web applications, and mobile apps that drive your success.",
            },
        ],
        arrPortfolio: [],
        indexCarousel: 0,
    })

    useEffect(() => {
        // const starCountRef = ref(db, 'Projects');
        const topUserPostsRef = query(
            ref(db, 'Projects/'),
            orderByChild('category'),
            equalTo('mobile-app'),
            limitToLast(6)
        )
        var arr: any[] = []
        onValue(topUserPostsRef, (snapshot) => {
            snapshot.forEach((childSnapshot) => {
                var childData = childSnapshot.val()
                arr.push(childData)
            })
            setState({ arrPortfolio: arr.filter((e) => e.status !== false) })
        })
    }, [setState])
    const handleSelect = (selectedIndex: number) => {
        setState({
            indexCarousel: selectedIndex,
        })
        // setIndex(selectedIndex);
    }
    const { indexCarousel, arrayCarousel } = state
    return (
        <div>
            <Navbar />
            <section id="hero">
                <div
                    id="heroCarousel"
                    data-bs-interval="5000"
                    className="carousel slide carousel-fade"
                    data-bs-ride="carousel"
                >
                    <Carousel
                        activeIndex={indexCarousel}
                        onSelect={(e) => handleSelect(e)}
                    >
                        {arrayCarousel.map((e, i) => {
                            return (
                                <div className="carousel-item" key={i}>
                                    <Image
                                        src={e.image}
                                        alt={''}
                                        //  width={500} height={500}
                                        layout="fill"
                                        objectFit="cover"
                                        objectPosition="center"
                                    />
                                    <div className="carousel-container">
                                        <div className="carousel-content animate__animated animate__fadeInUp">
                                            <h2>{e.caption}</h2>
                                            <p>{e.description}</p>
                                            <div className="text-center">
                                                <Link
                                                    href="/about"
                                                    className="btn-get-started"
                                                >
                                                    Read More
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        })}
                    </Carousel>
                </div>
                {/* <div className="waves"></div> */}
            </section>
            <section id="counts" className="counts">
                <div className="container" data-aos="fade-up">
                    <div className="row gy-4">
                        <div className="col-lg-3 col-md-6">
                            <div className="count-box">
                                <i
                                    className="bi bi-emoji-smile"
                                    style={{ color: '#7666df' }}
                                ></i>
                                <div>
                                    <div className="d-flex ">
                                        {/* <span data-purecounter-start="0" data-purecounter-end="232" data-purecounter-duration="1" className="purecounter"></span> */}
                                        <CountUp end={14} />
                                        <span
                                            style={{
                                                fontSize: '36px',
                                                display: 'block',
                                                fontWeight: 600,
                                                color: '#7666df',
                                                marginLeft: 5,
                                            }}
                                        >
                                            +
                                        </span>
                                    </div>
                                    <p>Happy Clients</p>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-3 col-md-6">
                            <div className="count-box">
                                <i
                                    className="bi bi-journal-richtext"
                                    style={{ color: '#7666df' }}
                                ></i>
                                <div>
                                    <div className="d-flex ">
                                        {/* <span data-purecounter-start="0" data-purecounter-end="232" data-purecounter-duration="1" className="purecounter"></span> */}
                                        <CountUp end={20} />
                                        <span
                                            style={{
                                                fontSize: '36px',
                                                display: 'block',
                                                fontWeight: 600,
                                                color: '#7666df', marginLeft: 5,
                                            }}
                                        >
                                            +
                                        </span>
                                    </div>
                                    <p>Projects</p>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-3 col-md-6">
                            <div className="count-box">
                                <i
                                    className="bi bi-headset"
                                    style={{ color: '#7666df' }}
                                >
                                    {' '}
                                </i>
                                <div>
                                    <div className="d-flex ">
                                        {/* <span data-purecounter-start="0" data-purecounter-end="232" data-purecounter-duration="1" className="purecounter"></span> */}
                                        <CountUp end={799} />
                                        <span
                                            style={{
                                                fontSize: '36px',
                                                display: 'block',
                                                fontWeight: 600,
                                                color: '#7666df', marginLeft: 5,
                                            }}
                                        >
                                            +
                                        </span>
                                    </div>
                                    <p>Hours Of Support</p>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-3 col-md-6">
                            <div className="count-box">
                                <i
                                    className="bi bi-people"
                                    style={{ color: '#7666df' }}
                                ></i>
                                <div>
                                    <div className="d-flex ">
                                        {/* <span data-purecounter-start="0" data-purecounter-end="232" data-purecounter-duration="1" className="purecounter"></span> */}
                                        <CountUp end={4} />
                                        <span
                                            style={{
                                                fontSize: '36px',
                                                display: 'block',
                                                fontWeight: 600,
                                                color: '#7666df', marginLeft: 5,
                                            }}
                                        >
                                            +
                                        </span>
                                    </div>
                                    <p>Years of experience</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <ServiceComponent />
            <section id="portfolio" className="portfolio">
                <div className="container-fluid">
                    <h2 className="text-center">
                        My <strong>Recent Work</strong>
                    </h2>
                    <p className="text-center mb-5">
                        Here are a few recent design projects
                    </p>

                    <div className="row portfolio-container" data-aos="fade-up">
                        {state.arrPortfolio.map((e, i) => {
                            return (
                                <div
                                    className="col-lg-4 col-md-6 col-sm-6 col-xs-12"
                                    key={i}
                                >
                                    <div className="portfolio-item filter-web">
                                        <Link
                                            href={`/portfolio/${e.newPostKey}`}
                                            title={e.headingName}
                                        >
                                            <div className="portfolio-wrap">
                                                <Image
                                                    placeholder="blur"
                                                    blurDataURL={e.downloadURL}
                                                    src={e.downloadURL}
                                                    className="thumbnail img-fluid"
                                                    // fill
                                                    style={{
                                                        height: '18rem',
                                                        width: '100%',
                                                        // objectFit: 'contain',
                                                    }}
                                                    alt=""
                                                    width={500}
                                                    height={500}
                                                />
                                                <div className="portfolio-info">
                                                    <h4>{e.headingName}</h4>
                                                    <p>
                                                        {e.category ===
                                                        'web-apps'
                                                            ? 'WEB APP'
                                                            : e.category ===
                                                              'mobile-app'
                                                            ? 'Mobile App'
                                                            : ''}
                                                    </p>

                                                    <div className="portfolio-links">
                                                        {/* <i className="bx bx-plus"></i> */}

                                                        <Link
                                                            href={`/portfolio/${e.newPostKey}`}
                                                            title={
                                                                e.headingName
                                                            }
                                                        >
                                                            <i className="bi bi-link-45deg"></i>
                                                        </Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </Link>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </section>
            <ClientComponent params={{
                id: ''
            }} />
            <Footer />
        </div>
    )
}
export default Page

//  {/* <div
//         > */}
//         <div style={{ display: 'flex', justifyContent: 'center', alignSelf: 'center', padding: '30vh', position: 'absolute', }}>
//           <TypeAnimation
//             sequence={[
//               'One', // Types 'One'
//               1000, // Waits 1s
//               'Two', // Deletes 'One' and types 'Two'
//               2000, // Waits 2s
//               'Two Three', // Types 'Three' without deleting 'Two'
//               () => {
//                 console.log('Sequence completed');
//               },
//             ]}
//             wrapper="span"
//             cursor={true}
//             repeat={Infinity}
//             style={{ fontSize: '33px', display: 'inline-block', textAlign: 'center', color: '#000' }}
//           />
//           {/* </div> */}
//

//         </div>

// <div className="backwrap gradient " >
// <div
// // className="d-flex align-items-center justify-content-around"
// // style={{}}
// >
//   <div
//     className="container d-flex justify-content-around"
//     style={{ height: '60vh', position: 'absolute', }}
//   >
//     <div className="row">
//       <div
//         className="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center"
//         data-aos="fade-up"
//         data-aos-delay="200"
//       >

//         <div
//           id="simpleUsage"
//         // style={{ fontSize: "2rem " }}
//         >
//           <h1
//             style={{
//               // backgroundColor: 'black',
//               color: "white",
//               // WebkitTextFillColor: "transparent",
//               WebkitTextStroke: "1px #7666df",
//             }}
//           >
//             Build Awesome apps with{" "}
//             <TypeAnimation
//               sequence={[
//                 // 'One', // Types 'One'
//                 // 1000, // Waits 1s
//                 // 'Two', // Deletes 'One' and types 'Two'
//                 // 2000, // Waits 2s
//                 // 'Two Three', // Types 'Three' without deleting 'Two'
//                 "React", 1000,
//                 "Node.js", 1000,
//                 "React Native", 1000,
//                 "PWA", 1000,
//                 // "React Native Expo",
//                 "Expo", 1000,
//                 "Firebase", 1000,
//                 "Material UI", 1000,
//                 "React Native Paper", 1000,
//                 "Bootstrap", 1000,
//                 // () => {
//                 //   console.log('Sequence completed');
//                 // },
//               ]}
//               wrapper="span"
//               cursor={true}
//               repeat={Infinity}
//               style={{ fontSize: '33px', display: 'inline-block', textAlign: 'center', color: '#fff' }}
//             />
//           </h1>
//         </div>
//         <p
//           style={{
//             // backgroundColor: 'black',
//             color: "#fff",
//             // WebkitTextFillColor: "transparent",
//             // WebkitTextStroke: "0.5px white",
//             // border: "none",
//             // fontWeight: 100,
//             // WebkitTextStroke: "1px #7666df96",
//             fontSize: "1.2rem",
//           }}
//         >
//           We are team of talented designers and developers for making
//           websites, Web Application & Mobile Apps
//         </p>

//         <div className="d-flex justify-content-center justify-content-lg-start">
//           <button type="button" className="btn " style={{ backgroundColor: '#7666df', color: '#fff', }} onClick={() => alert('asd')}>Let's Connect!</button>
//         </div>
//       </div>
//       <div
//         className="col-lg-6 order-1 order-lg-2 hero-img"
//         data-aos="zoom-in"
//         data-aos-delay="200"
//       >

//       </div>
//     </div>
//   </div>
// </div>
// <div className="back-shapes">
//   <span className="floating circle" style={{ top: "66.59856996935649%", left: " -13.020833333333334%", animationDelay: " -0.9s" }}></span>
//   <span className="floating triangle" style={{ top: "31.46067415730337%", left: "53.59375%", animationDelay: " -4.8s" }}></span>
//   <span className="floating cross" style={{ top: "76.50663942798774%", left: "38.020833333333336%", animationDelay: "-4s" }}></span>
//   <span className="floating square" style={{ top: "21.450459652706844%", left: "90.0625%", animationDelay: " -2.8s" }}></span>
//   <span className="floating square" style={{ top: "58.12053115423902%", left: "56.770833333333336%", animationDelay: "-2.15s" }}></span>
//   <span className="floating square" style={{ top: "8.682328907048008%", left: "72.70833333333333%", animationDelay: "-1.9s" }}></span>
//   <span className="floating cross" style={{ top: "31.3585291113381%", left: "58.541666666666664%", animationDelay: "-0.65s" }}></span>
//   <span className="floating cross" style={{ top: "69.96935648621042%", left: "81.45833333333333%", animationDelay: "-0.4s" }}></span>
//   <span className="floating circle" style={{ top: "15.117466802860061%", left: "59.34375%", animationDelay: "-4.1s;" }}></span>
//   <span className="floating circle" style={{ top: "13.074565883554648%", left: "45.989583333333336%", animationDelay: "-3.65s;" }}></span>
//   <span className="floating cross" style={{ top: "55.87334014300306%", left: "27.135416666666668%", animationDelay: "-2.25s;" }}></span>
//   <span className="floating cross" style={{ top: "49.54034729315628%", left: "87.75%", animationDelay: "-2s;" }}></span>
//   <span className="floating cross" style={{ top: "34.62717058222676%", left: "49.635416666666664%", animationDelay: "-1.55s;" }}></span>
//   <span className="floating cross" style={{ top: "33.19713993871297%", left: "86.14583333333333%", animationDelay: "-0.95s;" }}></span>
//   <span className="floating square" style={{ top: "28.19203268641471%", left: "82.208333%", animationDelay: "-4.45s;" }}></span>
//   <span className="floating circle" style={{ top: "39.7344228804903%", left: "7.833333%", animationDelay: "-3.35s;" }}></span>
//   <span className="floating triangle" style={{ top: "77.83452502553627%", left: "24.427083333333332%", animationDelay: "-2.3s;" }}></span>
//   <span className="floating triangle" style={{ top: "2.860061287027579%", left: "47.864583333333336%", animationDelay: "-1.75s;" }}></span>
//   <span className="floating triangle" style={{ top: "71.3993871297242%", left: "66.45833333333333%", animationDelay: "-1.25s;" }}></span>
//   <span className="floating triangle" style={{ top: "31.256384065372828%", left: "76.92708333333333%", animationDelay: "-0.65s;" }}></span>
//   <span className="floating triangle" style={{ top: "46.47599591419816%", left: "38.90625%", animationDelay: "-0.35s;" }}></span>
//   <span className="floating cross" style={{ top: "3.4729315628192032%", left: "19.635416666666668%", animationDelay: "-4.3s;" }}></span>
//   <span className="floating cross" style={{ top: "3.575076608784474%", left: "6.25%", animationDelay: "-4.05s;" }}></span>
//   <span className="floating cross" style={{ top: "77.3237997957099%", left: "4.583333333333333%", animationDelay: "-3.75s;" }}></span>
//   <span className="floating cross" style={{ top: "0.9193054136874361%", left: "71.14583333333333%", animationDelay: "-3.3s;" }}></span>
//   <span className="floating square" style={{ top: "23.6976506639428%", left: "63.28125%", animationDelay: "-2.1s;" }}></span>
//   <span className="floating square" style={{ top: "81.30745658835546%", left: "45.15625%", animationDelay: "-1.75s" }}></span>
//   <span className="floating square" style={{ top: "60.9805924412666%", left: "42.239583333333336%", animationDelay: "-1.45s" }}></span>
//   <span className="floating square" style={{ top: "29.009193054136876%", left: "93.90625%", animationDelay: "-1.05s" }}></span>
//   <span className="floating square" style={{ top: "52.093973442288046%", left: "68.90625%", animationDelay: "-0.7s" }}></span>
//   <span className="floating square" style={{ top: "81.51174668028601%", left: "83.59375%", animationDelay: "-0.35s" }}></span>
//   <span className="floating square" style={{ top: "11.542390194075587%", left: "91.51041666666667%", animationDelay: "-0.1s" }}></span>

// </div>
// </div>
