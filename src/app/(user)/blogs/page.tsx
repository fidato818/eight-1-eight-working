'use client'
import React, { useState, useEffect, useLayoutEffect } from 'react'
import Image from 'next/image'

import {
    ref as ref_database,
    onValue,
    query,
    orderByChild,
    equalTo,
} from 'firebase/database'

import parse from 'html-react-parser'
import { useSetState } from 'ahooks'
import { auth, db } from '@/config/firebaseConfig'
import moment from 'moment'
import './style.module.css'
import Link from 'next/link'
import RecentPosts from '@/components/recent-posts'
const BlogsPage = () => {
    const [state, setState] = useSetState<any | null>({
        headingName: '',
        category: 'web-apps',
        status: false,
        body: '',
        githubLink: '',
        buttonLink: '',
        arr: [],
        image: null,
        url: '',
        progress: 0,
        currentPage: 0,
        pageSize: 5,
        toggleEditImage: false,
        visiableError: false,
        modalShow: false,
        editData: false,
        errorImage: false,
        modalDetail: false,
        loaderToggle: false,
        spinnerLoader: false,
        visible: false,
        errorMessage: '',
        downloadURL: '',
        newPostKey: '',
        createAt: '',
        updateAt: '',
        localImage: '',
        localImageKey: '',
        imageUpdate: '',
    })
    const [isClient, setIsClient] = useState(false)

    useEffect(() => {
        setIsClient(true)
    }, [])
    useLayoutEffect(() => {
        setState({ spinnerLoader: true })
        const getData = () => {
            // const allPostData = ref_database(db, 'blogsData')
            const allPostData = query(
                ref_database(db, 'blogsData'),
                orderByChild('status'),
                equalTo(true)
            )
            onValue(allPostData, (snapshot) => {
                var newArr: any[] = []

                snapshot.forEach((data) => {
                    var childData = data.val()
                    newArr.push(childData)
                })

                setState({ arr: newArr.reverse(), spinnerLoader: false })
            })
        }
        getData()
    }, [setState])
    return (
        <section id="blog" className="blog">
            {state.spinnerLoader ? (
                <>
                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                <div
                    className="spinner-grow"
                    style={{
                        width: '4rem',
                        height: '4rem',
                        color: '#7666df8a',
                    }}
                    role="status"
                >
                    {/* <span className="sr-only">Loading...</span> */}
                </div>
            </div>
                </>
            ) : (
                <>
                    {state.arr.length ? (
                        <div className="container" data-aos="fade-up">
                            <div className="row">
                                <div className="col-lg-8 entries">
                                    {state.arr.map((e: any, i: number) => {
                                        return (
                                            <div key={i}>
                                                <article className="entry">
                                                    <div
                                                        className="entry-Image"
                                                        style={{
                                                            maxHeight: '440px',
                                                            margin: '-30px -30px 20px -30px',
                                                            overflow: 'hidden',
                                                            // width: '100%',
                                                            // height: '50%',
                                                        }}
                                                    >
                                                        <Image
                                                            src={e.downloadURL}
                                                            alt=""
                                                            className="Image-fluid"
                                                            style={{
                                                                width: '100%',
                                                                height: 'auto',
                                                            }}
                                                            width={500}
                                                            height={1000}
                                                            // fill={true}
                                                        />
                                                    </div>

                                                    <h2 className="entry-title">
                                                        <a href="blog-single.html">
                                                            {e.headingName}
                                                        </a>
                                                    </h2>

                                                    <div className="entry-meta">
                                                        <ul>
                                                            <li className="d-flex align-items-center">
                                                                <i className="bi bi-person"></i>{' '}
                                                                <a href="blog-single.html">
                                                                    {
                                                                        e.buttonLink
                                                                    }
                                                                </a>
                                                            </li>
                                                            <li className="d-flex align-items-center">
                                                                <i className="bi bi-clock"></i>{' '}
                                                                <a href="blog-single.html">
                                                                    <time dateTime="2020-01-01">
                                                                        {/* Jan 1, 2020 */}
                                                                        {/* {e.createAt} */}
                                                                        {moment(
                                                                            e.createAt
                                                                        ).format(
                                                                            'MMM D, YYYY'
                                                                        )}
                                                                    </time>
                                                                </a>
                                                            </li>
                                                            {/* <li className="d-flex align-items-center">
                                                        <i className="bi bi-chat-dots"></i>{' '}
                                                        <a href="blog-single.html">
                                                            12 Comments
                                                        </a>
                                                    </li> */}
                                                        </ul>
                                                    </div>

                                                    <div className="entry-content">
                                                        <p>
                                                            {parse(
                                                                e.body.substring(
                                                                    0,
                                                                    200
                                                                )
                                                            )}
                                                        </p>
                                                        <div className="read-more">
                                                            <Link
                                                                href={`blogs/${e.newPostKey}`}
                                                            >
                                                                Read More
                                                            </Link>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                        )
                                    })}

                                    {/* <div className="blog-pagination">
                                <ul className="justify-content-center">
                                    <li>
                                        <a href="#">1</a>
                                    </li>
                                    <li className="active">
                                        <a href="#">2</a>
                                    </li>
                                    <li>
                                        <a href="#">3</a>
                                    </li>
                                </ul>
                            </div> */}
                                </div>

                                <div className="col-lg-4">
                                    <div className="sidebar">
                                        {/* <h3 className="sidebar-title">Search</h3>
                                <div className="sidebar-item search-form">
                                    <form action="">
                                        <input type="text" />
                                        <button type="submit">
                                            <i className="bi bi-search"></i>
                                        </button>
                                    </form>
                                </div> */}

                                        <h3 className="sidebar-title">
                                            Categories
                                        </h3>
                                        <div className="sidebar-item categories">
                                            <ul>
                                                <li>
                                                    <a href="#">
                                                        General{' '}
                                                        <span>(25)</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        Lifestyle{' '}
                                                        <span>(12)</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        Travel <span>(5)</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        Design <span>(22)</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        Creative{' '}
                                                        <span>(8)</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        Education{' '}
                                                        <span>(14)</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>

                                        <h3 className="sidebar-title">
                                            Recent Posts
                                        </h3>
                                        <div className="sidebar-item recent-posts">
                                            <RecentPosts />

                                            {/* <div className="post-item clearfix">
                                        <Image
                                            src="assets/Image/blog/blog-recent-2.jpg"
                                            alt=""
                                            width={100}
                                            height={100}
                                        />
                                        <h4>
                                            <a href="blog-single.html">
                                                Quidem autem et impedit
                                            </a>
                                        </h4>
                                        <time dateTime="2020-01-01">
                                            Jan 1, 2020
                                        </time>
                                    </div>

                                    <div className="post-item clearfix">
                                        <Image
                                            src="assets/Image/blog/blog-recent-3.jpg"
                                            alt=""
                                            width={100}
                                            height={100}
                                        />
                                        <h4>
                                            <a href="blog-single.html">
                                                Id quia et et ut maxime
                                                similique occaecati ut
                                            </a>
                                        </h4>
                                        <time dateTime="2020-01-01">
                                            Jan 1, 2020
                                        </time>
                                    </div>

                                    <div className="post-item clearfix">
                                        <Image
                                            src="assets/Image/blog/blog-recent-4.jpg"
                                            alt=""
                                            width={100}
                                            height={100}
                                        />
                                        <h4>
                                            <a href="blog-single.html">
                                                Laborum corporis quo dara net
                                                para
                                            </a>
                                        </h4>
                                        <time dateTime="2020-01-01">
                                            Jan 1, 2020
                                        </time>
                                    </div>

                                    <div className="post-item clearfix">
                                        <Image
                                            src="assets/Image/blog/blog-recent-5.jpg"
                                            alt=""
                                            width={100}
                                            height={100}
                                        />
                                        <h4>
                                            <a href="blog-single.html">
                                                Et dolores corrupti quae illo
                                                quod dolor
                                            </a>
                                        </h4>
                                        <time dateTime="2020-01-01">
                                            Jan 1, 2020
                                        </time>
                                    </div> */}
                                        </div>

                                        <h3 className="sidebar-title">Tags</h3>
                                        <div className="sidebar-item tags">
                                            <ul>
                                                <li>
                                                    <a href="#">App</a>
                                                </li>
                                                <li>
                                                    <a href="#">IT</a>
                                                </li>
                                                <li>
                                                    <a href="#">Business</a>
                                                </li>
                                                <li>
                                                    <a href="#">Mac</a>
                                                </li>
                                                <li>
                                                    <a href="#">Design</a>
                                                </li>
                                                <li>
                                                    <a href="#">Office</a>
                                                </li>
                                                <li>
                                                    <a href="#">Creative</a>
                                                </li>
                                                <li>
                                                    <a href="#">Studio</a>
                                                </li>
                                                <li>
                                                    <a href="#">Smart</a>
                                                </li>
                                                <li>
                                                    <a href="#">Tips</a>
                                                </li>
                                                <li>
                                                    <a href="#">Marketing</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ) : (
                        <div className="container">
                            <div
                                className=" "
                                style={{
                                    padding: '5rem',
                                    border: '2px dashed #7666df',
                                }}
                            >
                                <h1 className="text-center">
                                    Blogs not available
                                </h1>
                            </div>
                        </div>
                    )}
                </>
            )}
            
        </section>
    )
}

export default BlogsPage
