'use client'
import { useEffect, useCallback } from 'react'
import { useSetState } from 'ahooks'
import { ref, onValue, query, equalTo, orderByChild } from 'firebase/database'
import { useSearchParams, usePathname, useRouter } from 'next/navigation'
import Image from 'next/image'
import Link from 'next/link'
import { db } from '@/config/firebaseConfig'

interface sample_arr {
    arrPortfolio: {
        body: string
        buttonLink: string
        category: string
        createAt: string
        currentUser: string
        downloadURL: string
        githubLink: string
        headingName: string
        newPostKey: string
        status: boolean
        updateAt: string
    }[]
}

const Portfolio = () => {
    const pathname = usePathname()
    const router = useRouter()
    const searchParams = useSearchParams()!
    const selectedCategory = searchParams.get('selectedCategory')
    const [state, setState] = useSetState<sample_arr>({
        arrPortfolio: [],
    })

    const createQueryString = useCallback(
        (name: string, value: string) => {
            const params = new URLSearchParams(searchParams)
            params.set(name, value)
            return params.toString()
        },
        [searchParams]
    )

    useEffect(() => {
        const starCountRef = query(
            ref(db, 'Projects'),
            orderByChild('status'),
            equalTo(true)
        )
        var arr: any[] = []
        onValue(starCountRef, (snapshot) => {
            snapshot.forEach((childSnapshot) => {
                var childData = childSnapshot.val()
                arr.push(childData)
            })
            setState({ arrPortfolio: arr })
        })
    }, [setState])

    const { arrPortfolio } = state
    const checkCategory = selectedCategory === null ? 'all' : selectedCategory
    const filteredMoviesByGenre =
        checkCategory === 'all'
            ? arrPortfolio
            : arrPortfolio.filter((movie) =>
                  movie.category.includes(checkCategory)
              )
    const textSearchArr = ['all', 'web-apps', 'mobile-app']

    return (
        <main>
            <section id="portfolio" className="portfolio">
                <div className="container-fluid">
                    <div className="row" data-aos="fade-up">
                        <div className="col-lg-12 d-flex justify-content-center">
                            <ul id="portfolio-flters">
                                <li
                                    data-filter="*"
                                    className={
                                        textSearchArr.find(
                                            (el) => el === checkCategory
                                        ) === 'all'
                                            ? 'filter-active'
                                            : ''
                                    }
                                    onClick={() => {
                                        router.push(
                                            pathname +
                                                '?' +
                                                createQueryString(
                                                    'selectedCategory',
                                                    'all'
                                                )
                                        )
                                    }}
                                >
                                    All
                                </li>

                                <li
                                    data-filter=".filter-card "
                                    className={
                                        textSearchArr.find(
                                            (el) => el === checkCategory
                                        ) === 'web-apps'
                                            ? 'filter-active'
                                            : ''
                                    }
                                    onClick={() => {
                                        // <pathname>?sort=asc
                                        router.push(
                                            pathname +
                                                '?' +
                                                createQueryString(
                                                    'selectedCategory',
                                                    'web-apps'
                                                )
                                        )
                                    }}
                                >
                                    Web App
                                </li>
                                <li
                                    data-filter=".filter-web"
                                    className={
                                        textSearchArr.find(
                                            (el) => el === checkCategory
                                        ) === 'mobile-app'
                                            ? 'filter-active'
                                            : ''
                                    }
                                    onClick={() => {
                                        // <pathname>?sort=asc
                                        router.push(
                                            pathname +
                                                '?' +
                                                createQueryString(
                                                    'selectedCategory',
                                                    'mobile-app'
                                                )
                                        )
                                    }}
                                >
                                    Mobile App
                                </li>
                                <li
                                    data-filter=".filter-web"
                                    className={
                                        textSearchArr.find(
                                            (el) => el === checkCategory
                                        ) === 'web-app-with-mobile-app'
                                            ? 'filter-active'
                                            : ''
                                    }
                                    onClick={() => {
                                        // <pathname>?sort=asc
                                        router.push(
                                            pathname +
                                                '?' +
                                                createQueryString(
                                                    'selectedCategory',
                                                    'web-app-with-mobile-app'
                                                )
                                        )
                                    }}
                                >
                                  Web App With Mobile App
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div className="row portfolio-container" data-aos="fade-up">
                        {filteredMoviesByGenre.map((e: any, i: number) => {
                            return (
                                <div
                                    className="col-lg-4 col-md-6 col-sm-6 col-xs-12"
                                    key={i}
                                >
                                    <div className="portfolio-item filter-web">
                                        <Link
                                            href={`/portfolio/${e.newPostKey}`}
                                            title={e.headingName}
                                        >
                                            <div className="portfolio-wrap">
                                                <Image
                                                    placeholder="blur"
                                                    blurDataURL={e.downloadURL}
                                                    src={e.downloadURL}
                                                    className="thumbnail img-fluid"
                                                    // fill
                                                    style={{
                                                        height: '18rem',
                                                        width: '100%',
                                                        // objectFit: 'contain',
                                                    }}
                                                    alt=""
                                                    width={500}
                                                    height={500}
                                                />
                                                <div className="portfolio-info">
                                                    <h4>{e.headingName}</h4>
                                                    <p >
                                                       
                                                            {e.category.replaceAll('-', ' ')}
                                                    </p>

                                                    <div className="portfolio-links">
                                                        {/* <i className="bx bx-plus"></i> */}

                                                        <Link
                                                            href={`/portfolio/${e.newPostKey}`}
                                                            title={
                                                                e.headingName
                                                            }
                                                        >
                                                            <i className="bi bi-link-45deg"></i>
                                                        </Link>
                                                    </div>
                                                </div>
                                            </div>
                                        </Link>
                                    </div>
                                </div>
                            )
                        })}

                        {/* <div classNameName="col-lg-4 col-md-6 portfolio-item filter-web">
                            <img src="assets/img/portfolio/portfolio-2.jpg" classNameName="img-fluid" alt="" />
                            <div classNameName="portfolio-info">
                                <h4>Web 3</h4>
                                <p>Web</p>
                                <a href="assets/img/portfolio/portfolio-2.jpg" data-gallery="portfolioGallery" classNameName="portfolio-lightbox preview-link" title="Web 3"><i classNameName="bx bx-plus"></i></a>
                                <a href="portfolio-details.html" classNameName="details-link" title="More Details"><i classNameName="bx bx-link"></i></a>
                            </div>
                        </div>

                        <div classNameName="col-lg-4 col-md-6 portfolio-item filter-app">
                            <img src="assets/img/portfolio/portfolio-3.jpg" classNameName="img-fluid" alt="" />
                            <div classNameName="portfolio-info">
                                <h4>App 2</h4>
                                <p>App</p>
                                <a href="assets/img/portfolio/portfolio-3.jpg" data-gallery="portfolioGallery" classNameName="portfolio-lightbox preview-link" title="App 2"><i classNameName="bx bx-plus"></i></a>
                                <a href="portfolio-details.html" classNameName="details-link" title="More Details"><i classNameName="bx bx-link"></i></a>
                            </div>
                        </div>

                        <div classNameName="col-lg-4 col-md-6 portfolio-item filter-card">
                            <img src="assets/img/portfolio/portfolio-4.jpg" classNameName="img-fluid" alt="" />
                            <div classNameName="portfolio-info">
                                <h4>Card 2</h4>
                                <p>Card</p>
                                <a href="assets/img/portfolio/portfolio-4.jpg" data-gallery="portfolioGallery" classNameName="portfolio-lightbox preview-link" title="Card 2"><i classNameName="bx bx-plus"></i></a>
                                <a href="portfolio-details.html" classNameName="details-link" title="More Details"><i classNameName="bx bx-link"></i></a>
                            </div>
                        </div>

                        <div classNameName="col-lg-4 col-md-6 portfolio-item filter-web">
                            <img src="assets/img/portfolio/portfolio-5.jpg" classNameName="img-fluid" alt="" />
                            <div classNameName="portfolio-info">
                                <h4>Web 2</h4>
                                <p>Web</p>
                                <a href="assets/img/portfolio/portfolio-5.jpg" data-gallery="portfolioGallery" classNameName="portfolio-lightbox preview-link" title="Web 2"><i classNameName="bx bx-plus"></i></a>
                                <a href="portfolio-details.html" classNameName="details-link" title="More Details"><i classNameName="bx bx-link"></i></a>
                            </div>
                        </div>

                        <div classNameName="col-lg-4 col-md-6 portfolio-item filter-app">
                            <img src="assets/img/portfolio/portfolio-6.jpg" classNameName="img-fluid" alt="" />
                            <div classNameName="portfolio-info">
                                <h4>App 3</h4>
                                <p>App</p>
                                <a href="assets/img/portfolio/portfolio-6.jpg" data-gallery="portfolioGallery" classNameName="portfolio-lightbox preview-link" title="App 3"><i classNameName="bx bx-plus"></i></a>
                                <a href="portfolio-details.html" classNameName="details-link" title="More Details"><i classNameName="bx bx-link"></i></a>
                            </div>
                        </div>

                        <div classNameName="col-lg-4 col-md-6 portfolio-item filter-card">
                            <img src="assets/img/portfolio/portfolio-7.jpg" classNameName="img-fluid" alt="" />
                            <div classNameName="portfolio-info">
                                <h4>Card 1</h4>
                                <p>Card</p>
                                <a href="assets/img/portfolio/portfolio-7.jpg" data-gallery="portfolioGallery" classNameName="portfolio-lightbox preview-link" title="Card 1"><i classNameName="bx bx-plus"></i></a>
                                <a href="portfolio-details.html" classNameName="details-link" title="More Details"><i classNameName="bx bx-link"></i></a>
                            </div>
                        </div>

                        <div classNameName="col-lg-4 col-md-6 portfolio-item filter-card">
                            <img src="assets/img/portfolio/portfolio-8.jpg" classNameName="img-fluid" alt="" />
                            <div classNameName="portfolio-info">
                                <h4>Card 3</h4>
                                <p>Card</p>
                                <a href="assets/img/portfolio/portfolio-8.jpg" data-gallery="portfolioGallery" classNameName="portfolio-lightbox preview-link" title="Card 3"><i classNameName="bx bx-plus"></i></a>
                                <a href="portfolio-details.html" classNameName="details-link" title="More Details"><i classNameName="bx bx-link"></i></a>
                            </div>
                        </div>

                        <div classNameName="col-lg-4 col-md-6 portfolio-item filter-web">
                            <img src="assets/img/portfolio/portfolio-9.jpg" classNameName="img-fluid" alt="" />
                            <div classNameName="portfolio-info">
                                <h4>Web 3</h4>
                                <p>Web</p>
                                <a href="assets/img/portfolio/portfolio-9.jpg" data-gallery="portfolioGallery" classNameName="portfolio-lightbox preview-link" title="Web 3"><i classNameName="bx bx-plus"></i></a>
                                <a href="portfolio-details.html" classNameName="details-link" title="More Details"><i classNameName="bx bx-link"></i></a>
                            </div>
                        </div> */}
                    </div>
                </div>
            </section>
        </main>
    )
}
export default Portfolio
