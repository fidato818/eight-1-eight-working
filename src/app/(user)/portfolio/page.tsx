// app/page.tsx
import { Suspense } from 'react'
import MyComponent from './portfolio-comp'

export default function Page() {
    return (
        <div>
            
            <Suspense fallback={<>Loading...</>}>
                <MyComponent />
            </Suspense>
        </div>
    )
}
