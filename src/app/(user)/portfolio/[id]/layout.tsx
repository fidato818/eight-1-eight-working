import type { Metadata, ResolvingMetadata } from 'next'

import parse from 'html-react-parser'

// var metaTitle = '818 | Digital Agency | Services'
// var metaDescription =
//     '818 | Digital Agency is a cutting-edge firm specializing in web design, digital marketing, and tech solutions. We propel businesses into the online world with creativity, data-driven strategies, and measurable results. Partner with us to thrive in the dynamic digital landscape and achieve your online objectives with confidence.'
// export const metadata: Metadata = {
//     title: metaTitle,
//     description: metaDescription,
//     openGraph: {
//         title: metaTitle,
//         description: metaDescription,
//     },
// }

type Props = {
    params: { id: string }
    // searchParams?: { [key: string]: string | string[] | undefined }
    // searchParams?: { [key: string]: string | string[] | undefined | never }
}

export async function generateMetadata(
    // { params, searchParams }: Props,
    { params }: Props,
    parent: ResolvingMetadata
): Promise<Metadata> {
    const id = params.id

    const urlString = `https://adnan-ahmed.firebaseio.com/Projects.json?orderBy=\"newPostKey\"&equalTo=\"${id}\"`
    const product = await fetch(urlString)
        .then((res) => res.json())
        .then((e) => Object.values(e)[0])

    const { headingName, body, downloadURL }: any = product
    const getBody: any = body !== undefined && parse(body.substring(0, 100))
    const { props } = getBody || {}
    const descBody =
        props !== undefined
            ? props.children
            : getBody[0] && getBody[0].props.children

    // optionally access and extend (rather than replace) parent metadata
    const previousImages = (await parent).openGraph?.images || []

    return {
        title: `${headingName} | 818 | Digital Agency`,
        description:
            descBody === undefined ? 'Description is not Available' : descBody,
        openGraph: {
            title: `${headingName} | 818 | Digital Agency`,
            description:
                descBody === undefined
                    ? 'Description is not Available'
                    : descBody,
            // images: ['/some-specific-page-image.jpg', ...previousImages],
            // images: [product.downloadURL, ...previousImages],
            images: [downloadURL],
        },
    }
}

const ServiceLayout = ({ children }: { children: React.ReactNode }) => {
    return <>{children}</>
}
export default ServiceLayout
