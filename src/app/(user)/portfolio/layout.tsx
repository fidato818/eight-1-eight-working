import type { Metadata } from 'next'



var metaTitle = 'Portfolio | 818 | Digital Agency'
var metaDescription =
    '818 | Digital Agency is a cutting-edge firm specializing in web design, digital marketing, and tech solutions. We propel businesses into the online world with creativity, data-driven strategies, and measurable results. Partner with us to thrive in the dynamic digital landscape and achieve your online objectives with confidence.'
export const metadata: Metadata = {
    title: metaTitle,
    description: metaDescription,
    openGraph: {
        title: metaTitle,
        description: metaDescription,
    },
}
const ServiceLayout = ({ children }: { children: React.ReactNode }) => {
    return <>{children}</>
}
export default ServiceLayout
