'use client'
import React from 'react'
import moment from 'moment'
import { useSetState } from 'ahooks'
// import { ref, child, push } from 'firebase/database'
// import { db } from '@/config/firebaseConfig'

import emailjs from '@emailjs/browser'
const PROECT = () => {
    const [state, setState] = useSetState<any | null>({
        projectType: 'website-or-web-app',
    })

    const radioHandler = (event: any) => {
        setState({
            projectType: event.target.value,
        })
    }
    const changeHandler = (
        event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
    ) => {
        event.preventDefault()
        const { name, value } = event.target
        let obj: any = { [name]: value }
        setState(obj)
    }
    const submitHandler = (e: { preventDefault: () => void }) => {
        e.preventDefault()
        setState({ loadToggle: true })
        const {
            clientName,
            clientEmail,
            clientCompany,
            clientPhone,
            projectType,
            projectDetail,
            pageOutline,
            primaryObjective,
            clientBudget,
            clientDeadline,
            pagesRequired,
            featureAndFunctionality,
        } = state

        var obj = {
            clientName,
            clientEmail,
            clientCompany,
            clientPhone,
            projectType,
            projectDetail,
            pageOutline,
            primaryObjective,
            clientBudget: `$${Number(clientBudget)}.00`,
            clientDeadline: moment(clientDeadline).format('DD-MM-YYYY'),
            pagesRequired,
            featureAndFunctionality,
            createAt: new Date(),
            // plannerId,
        }

        sendEmailtoMe(obj)
    }

    const sendEmailtoMe = (objData: any, count?: any) => {
        const {
            clientName,
            clientEmail,
            clientCompany,
            clientPhone,
            clientBudget,
            clientDeadline,
            createAt,
            projectType,
            projectDetail,
            pageOutline,
            primaryObjective,
            pagesRequired,
            featureAndFunctionality,
        } = objData
        // const plannerId = push(child(ref(db), 'Project_Planner')).key
        const templateParams = {
            full_name: clientName,
            from_email: clientEmail,
            // order_id: plannerId,
            from_phone: clientPhone,
            client_company: clientCompany,
            client_budget: clientBudget,
            client_deadline: clientDeadline,
            create_at: new Date(createAt).toLocaleString(),
            message: `Client Name: ${clientName} 

                    Client Email: ${clientEmail} 

                    Client Company: ${clientCompany}  

                    Client Phone: ${clientPhone} 

                    TYPE OF PROJECT: ${projectType} 

                    Tell us about your company: ${projectDetail} 

                    Could you please outline the pages or specific requirements you have in mind for your project?: ${pageOutline} 

                    What are the primary objectives and desired outcomes for the website?: ${primaryObjective} 

                    Client Budget: ${clientBudget} 

                    Client Deadline: ${clientDeadline} 

                    Could you please outline the pages or specific requirements?: ${pagesRequired} 

                    What features & functionalities are you looking in your website?: ${featureAndFunctionality} 
                    `,
        }
  

        emailjs
            .send(
                'gmail_edell5400',
                'template_pa2iisv',
                templateParams,
                '5I3ybAUOdfGjRASHh'
                // "gmail",
                // "template_default",
                // // e.target,
                // "#myForm",
                // "user_9GNAql6wVXgFLUtYx6xjP"
            )
            .then(
                (response) => {
                    // console.log("SUCCESS!", response.status, response.text);
                    console.log('SUCCESS!')
                    setState({ loadToggle: false })
                },
                (err) => {
                    console.log('FAILED...', err)
                }
            )
    }
    return (
        <div className="container">
            {state.loadToggle === false ? (
                <section>
                    <h4
                        className="text-center "
                        style={{
                            border: '4px dashed #7666df',
                            padding: '5rem',
                        }}
                    >
                        Thank you for reaching out to us. We&apos;ve received
                        your quotation and will ensure a prompt response. Our
                        team is dedicated to providing you with the assistance
                        and information you need, and you can expect to hear
                        back from us shortly. If you have any additional details
                        or questions in the meantime, please don&apos;t hesitate
                        to get in touch.
                    </h4>
                </section>
            ) : (
                <>
                    <h1 className="mt-4 text-center">Project Planner</h1>
                    <h5 className="mb-4 text-center">
                        Are you interested in collaborating with us on a
                        project? Please provide us with all the project details
                        below, and we will promptly respond to your inquiry
                    </h5>

                    <section id="services" className="services">
                        <h4 className="mt-4 text-uppercase">
                            Your Business Details
                        </h4>
                        <div className="">
                            <form
                                className="row g-3 m-1"
                                onSubmit={submitHandler}
                            >
                                <div className="col-md-6 mb-3">
                                    <label
                                        htmlFor="exampleInputEmail1"
                                        className="form-label"
                                    >
                                        Name
                                    </label>
                                    <input
                                        required
                                        placeholder=""
                                        type="text"
                                        className="form-control"
                                        id="exampleInputEmail1"
                                        aria-describedby="emailHelp"
                                        name="clientName"
                                        onChange={changeHandler}
                                    />
                                    {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}
                                </div>
                                <div className="col-md-6 mb-3">
                                    <label
                                        htmlFor="exampleInputEmail1"
                                        className="form-label"
                                    >
                                        Email address
                                    </label>
                                    <input
                                        required
                                        type="email"
                                        className="form-control"
                                        id="exampleInputEmail1"
                                        aria-describedby="emailHelp"
                                        name="clientEmail"
                                        onChange={changeHandler}
                                    />
                                    {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}
                                </div>
                                <div className="col-md-6 mb-3">
                                    <label
                                        htmlFor="exampleInputEmail1"
                                        className="form-label"
                                    >
                                        Company
                                    </label>
                                    <input
                                        aria-required
                                        required
                                        type="text"
                                        className="form-control"
                                        id="exampleInputEmail1"
                                        aria-describedby="emailHelp"
                                        name="clientCompany"
                                        onChange={changeHandler}
                                    />
                                    {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}
                                </div>
                                <div className="col-md-6 mb-3">
                                    <label
                                        htmlFor="clientPhone"
                                        className="form-label"
                                    >
                                        Phone Number
                                    </label>
                                    <input
                                        required
                                        type="number"
                                        className="form-control"
                                        id="clientPhone"
                                        aria-describedby="emailHelp"
                                        name="clientPhone"
                                        onChange={changeHandler}
                                    />
                                </div>
                                <h4 className="mt-4 text-uppercase">
                                    Type Of Project
                                </h4>
                                <div className="col-md-12 mb-3 ">
                                    <input
                                        type="radio"
                                        className="btn-check"
                                        name="projectType"
                                        id="website-or-web-app"
                                        autoComplete="off"
                                        value="website-or-web-app"
                                        onClick={radioHandler}
                                        checked={
                                            state.projectType ==
                                            'website-or-web-app'
                                        }
                                    />
                                    <label
                                        className="btn btn-primary-success me-2"
                                        htmlFor="website-or-web-app"
                                    >
                                        Website / Web App
                                    </label>

                                    <input
                                        type="radio"
                                        className="btn-check"
                                        name="projectType"
                                        id="mobile-app"
                                        value="mobile-app"
                                        autoComplete="off"
                                        onClick={radioHandler}
                                        checked={
                                            state.projectType == 'mobile-app'
                                        }
                                    />
                                    <label
                                        className="btn btn-primary-danger me-2"
                                        htmlFor="mobile-app"
                                    >
                                        Mobile App
                                    </label>

                                    <input
                                        type="radio"
                                        className="btn-check"
                                        name="projectType"
                                        id="web-with-mobile-app"
                                        value="web-with-mobile-app"
                                        autoComplete="off"
                                        onChange={radioHandler}
                                        checked={
                                            state.projectType ==
                                            'web-with-mobile-app'
                                        }
                                    />
                                    <label
                                        className="btn btn-primary-success me-2"
                                        htmlFor="web-with-mobile-app"
                                    >
                                        Web with Mobile App
                                    </label>

                                    <input
                                        type="radio"
                                        className="btn-check"
                                        name="projectType"
                                        id="something-else"
                                        value="something-else"
                                        autoComplete="off"
                                        onChange={radioHandler}
                                        checked={
                                            state.projectType ==
                                            'something-else'
                                        }
                                    />
                                    <label
                                        className="btn btn-primary-danger"
                                        htmlFor="something-else"
                                    >
                                        Something Else
                                    </label>
                                </div>

                                <h4 className="mt-4 text-uppercase">
                                    Brief Description
                                </h4>

                                <div className="col-md-4 mb-3">
                                    <label
                                        htmlFor="projectDetail"
                                        className="form-label"
                                    >
                                        Tell us about your company.
                                    </label>

                                    <textarea
                                        required
                                        rows={7}
                                        placeholder="Share the details, and we'll gain a better understanding of your organization to explore potential collaboration opportunities"
                                        className="form-control"
                                        id="projectDetail"
                                        aria-describedby="emailHelp"
                                        name="projectDetail"
                                        onChange={changeHandler}
                                    ></textarea>

                                    {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}
                                </div>
                                <div className="col-md-4 mb-3">
                                    <label
                                        htmlFor="exampleInputEmail1"
                                        className="form-label"
                                    >
                                        Could you please outline the pages or
                                        specific requirements you have in mind
                                        for your project?
                                    </label>
                                    <textarea
                                        required
                                        rows={6}
                                        placeholder="Please outline the aims of the project for our review"
                                        className="form-control"
                                        id="exampleInputEmail1"
                                        aria-describedby="emailHelp"
                                        name="pageOutline"
                                        onChange={changeHandler}
                                    ></textarea>
                                    {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}
                                </div>
                                <div className="col-md-4 mb-3">
                                    <label
                                        htmlFor="exampleInputEmail1"
                                        className="form-label"
                                    >
                                        What are the primary objectives and
                                        desired outcomes for the website?
                                    </label>
                                    <textarea
                                        required
                                        rows={6}
                                        placeholder="Please specify the key goals that the website should achieve"
                                        className="form-control"
                                        id="exampleInputEmail1"
                                        aria-describedby="emailHelp"
                                        name="primaryObjective"
                                        onChange={changeHandler}
                                    ></textarea>
                                </div>

                                <div className="col-md-6">
                                    <label
                                        htmlFor="exampleInputEmail1"
                                        className="form-label"
                                    >
                                        Budget
                                    </label>
                                    <div className="input-group  mb-3">
                                        <span className="input-group-text">
                                            $
                                        </span>
                                        <input
                                            required
                                            type="number"
                                            className="form-control"
                                            id="exampleInputEmail1"
                                            aria-describedby="emailHelp"
                                            name="clientBudget"
                                            onChange={changeHandler}
                                            aria-label="Amount (to the nearest dollar)"
                                        />
                                        <span className="input-group-text">
                                            .00
                                        </span>
                                    </div>
                                </div>

                                <div className="col-md-6 mb-3">
                                    <label
                                        htmlFor="exampleInputEmail1"
                                        className="form-label"
                                    >
                                        Deadline
                                    </label>
                                    <input
                                        min={
                                            new Date()
                                                .toISOString()
                                                .split('T')[0]
                                        }
                                        required
                                        type="date"
                                        className="form-control"
                                        id="exampleInputEmail1"
                                        aria-describedby="emailHelp"
                                        name="clientDeadline"
                                        onChange={changeHandler}
                                    />
                                </div>

                                <h4 className="mt-4 text-uppercase">
                                    Design & Development
                                </h4>
                                <div className="col-md-6 mb-3">
                                    <label
                                        htmlFor="exampleInputEmail1"
                                        className="form-label"
                                    >
                                        Could you please outline the pages or
                                        specific requirements?
                                    </label>

                                    <textarea
                                        required
                                        rows={4}
                                        placeholder="E.g. Home, About Us, Services, Blog, Portfolio, Contact Us"
                                        className="form-control"
                                        id="exampleInputEmail1"
                                        aria-describedby="emailHelp"
                                        name="pagesRequired"
                                        onChange={changeHandler}
                                    ></textarea>
                                    {/* <div id="emailHelp" className="form-text">E.g. Home, About Us, Services, Blog, Portfolio, Contact Us</div> */}
                                </div>
                                <div className="col-md-6 mb-3">
                                    <label
                                        htmlFor="exampleInputEmail1"
                                        className="form-label"
                                    >
                                        What features & functionalities are you
                                        looking in your website?{' '}
                                    </label>

                                    <textarea
                                        rows={4}
                                        // placeholder="Please specify the key goals that the website should achieve"
                                        className="form-control"
                                        id="exampleInputEmail1"
                                        aria-describedby="emailHelp"
                                        name="featureAndFunctionality"
                                        onChange={changeHandler}
                                    ></textarea>
                                </div>
                                {/* <div className="col-auto mb-3 form-check">
                            <input type="checkbox" className="form-check-input" id="exampleCheck1" />
                            <label className="form-check-label" htmlFor="exampleCheck1">Check me out</label>
                        </div> */}
                                <div className="d-flex justify-content-end">
                                    <button
                                        disabled={
                                            state.loadToggle === true
                                                ? true
                                                : false
                                        }
                                        type="submit"
                                        className="btn"
                                        style={{
                                            backgroundColor: '#7666df',
                                            color: '#fff',
                                        }}
                                    >
                                        Submit Your Requirements
                                    </button>
                                </div>
                            </form>
                        </div>
                    </section>
                </>
            )}
        </div>
    )
}

export default PROECT
