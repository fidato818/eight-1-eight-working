"use client"
import React from 'react'
import { useSetState } from 'ahooks'

import Image from 'next/image'
interface arraySvg {
    arrSvg: { imageLink: string, altName: string, heightImage: number, widthImage: number }[]
}

const MOBILEPAGE = () => {
    const [state, setState] = useSetState<arraySvg>(
        {
            arrSvg: [
                { imageLink: require('@/assets/images/icons8-react-native.svg'), altName: "react-native", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-javascript.svg'), altName: "javascript", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-nodejs.svg'), altName: "nodejs", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-mongodb.svg'), altName: "mongodb", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-firebase.svg'), altName: "firebase", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-express-js.svg'), altName: "express-js", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-expo.svg'), altName: "expo", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-redux.svg'), altName: "redux", heightImage: 100, widthImage: 100 }

            ]
        }
    )

    return (
        <div className='container'>
            <h1 className='mt-4'>Services</h1>
            <h5 className='mb-4'>We specializes in crafting innovative and high-quality mobile applications for businesses and individuals. With a dedicated team of experienced developers, designers, and project managers, we take your app ideas from concept to reality, delivering top-notch solutions that drive success and user engagement.</h5>
            <section id="services" className="services">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6 col-md-6">
                            <div className="icon-box" data-aos="fade-up">
                                <div className="icon">
                                    <i className="bi bi-phone"></i>
                                </div>

                                <h4 className="title">
                                    <a href="">Custom Mobile App Development</a>
                                </h4>
                                <p className="description">
                                    Our expert developers create custom mobile applications tailored to your unique business needs. Whether it&apos;s iOS, Android, or cross-platform development, we deliver apps that stand out in the market.
                                </p>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-6">
                            <div
                                className="icon-box"
                                data-aos="fade-up"
                                data-aos-delay="100"
                            >
                                <div className="icon">
                                    <i className="bi bi-people"></i>
                                </div>
                                <h4 className="title">
                                    <a href="">Consultation and Strategy</a>
                                </h4>
                                <p className="description">
                                    We partner with you to understand your goals, target audience, and market dynamics. Our strategic guidance helps you make informed decisions and create a roadmap for your app&apos;s success
                                </p>
                            </div>
                        </div>

                        <div className="col-lg-6 col-md-6">
                            <div
                                className="icon-box"
                                data-aos="fade-up"
                                data-aos-delay="200"
                            >
                                <div className="icon">
                                    <i className="bi bi-shield-check"></i>
                                </div>
                                <h4 className="title">
                                    <a href="">Quality Assurance</a>
                                </h4>
                                <p className="description">
                                    Rigorous testing and quality assurance processes are integral to our development cycle. We ensure your app is bug-free and meets the highest industry standards.
                                </p>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-6">
                            <div
                                className="icon-box"
                                data-aos="fade-up"
                                data-aos-delay="300"
                            >
                                <div className="icon">
                                    {/* <i className="bi bi-list-check"></i> */}
                                    <Image alt='' src={require('@/assets/images/icons8-multiple-devices-90.png')} width={30} height={30} />
                                </div>
                                <h4 className="title">
                                    <a href="">Cross-Platform Development</a>
                                </h4>
                                <p className="description">
                                    We excel in cross-platform development using frameworks like React Native, enabling you to reach a broader audience with a single codebase.
                                </p>
                            </div>
                        </div>
                        <h1 className='mt-4'>Our  <strong>Expertise</strong></h1>

                        <div className="row">
                            {state.arrSvg.map((e, i) => {
                                return (
                                    <div className="col-lg-3 col-md-6" key={i}>
                                        <div
                                            className="icon-box-1"
                                            data-aos="fade-up"
                                            data-aos-delay="300">
                                            <Image
                                                src={e.imageLink}
                                                alt={e.altName}
                                                width={e.heightImage}
                                                height={e.widthImage} />
                                        </div>
                                    </div>
                                )
                            })}


                        </div>
                        <h1 className='mt-4 '>Why <strong>Choose us</strong></h1>

                        <div className="row pt-4">
                            <div className="col-lg-4 col-md-6">
                                <div className="icon-box" data-aos="fade-up">
                                    <div className="icon">
                                        <i className="bi bi-phone"></i>
                                    </div>

                                    <h4 className="title">
                                        <a href="">Expertise</a>
                                    </h4>
                                    <p className="description">
                                        Our team comprises skilled developers and designers with extensive experience in the mobile app development industry.
                                    </p>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-6">
                                <div
                                    className="icon-box"
                                    data-aos="fade-up"
                                    data-aos-delay="100"
                                >
                                    <div className="icon">
                                        <i className="bi bi-people"></i>
                                    </div>
                                    <h4 className="title">
                                        <a href="">Client-Centric Approach</a>
                                    </h4>
                                    <p className="description">
                                        We prioritize your unique requirements, ensuring that the final product aligns perfectly with your vision and goals
                                    </p>
                                </div>
                            </div>

                            <div className="col-lg-4 col-md-6">
                                <div
                                    className="icon-box"
                                    data-aos="fade-up"
                                    data-aos-delay="200"
                                >
                                    <div className="icon">
                                        <i className="bi bi-shield-check"></i>
                                    </div>
                                    <h4 className="title">
                                        <a href="">Cutting-Edge Technology</a>
                                    </h4>
                                    <p className="description">
                                        We stay up-to-date with the latest technologies and trends, integrating them into your app for enhanced performance and functionality.
                                    </p>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-6">
                                <div
                                    className="icon-box"
                                    data-aos="fade-up"
                                    data-aos-delay="300"
                                >
                                    <div className="icon">
                                        {/* <i className="bi bi-list-check"></i> */}
                                        <Image alt='' src={require('@/assets/images/icons8-delivery-time-100.png')} width={30} height={30} />
                                    </div>
                                    <h4 className="title">
                                        <a href="">Timely Delivery</a>
                                    </h4>
                                    <p className="description">
                                        We understand the importance of deadlines. We work efficiently to deliver your app on time, without compromising on quality.
                                    </p>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-6">
                                <div
                                    className="icon-box"
                                    data-aos="fade-up"
                                    data-aos-delay="200"
                                >
                                    <div className="icon">
                                        <Image alt='' src={require('@/assets/images/icons8-communication-100.png')} width={40} height={40} />

                                    </div>
                                    <h4 className="title">
                                        <a href="">Transparent Communication</a>
                                    </h4>
                                    <p className="description">
                                        We maintain open and transparent communication throughout the development process, keeping you informed at every stage.
                                    </p>
                                </div>
                            </div>
                            <div className="col-lg-4 col-md-6">
                                <div
                                    className="icon-box"
                                    data-aos="fade-up"
                                    data-aos-delay="300"
                                >
                                    <div className="icon">
                                        {/* <i className="bi bi-list-check"></i> */}
                                        <Image alt='' src={require('@/assets/images/icons8-pricing-64.png')} width={30} height={30} />

                                    </div>
                                    <h4 className="title">
                                        <a href="">Affordable Pricing</a>
                                    </h4>
                                    <p className="description">
                                        Our competitive pricing ensures that you get exceptional value for your investment.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default MOBILEPAGE