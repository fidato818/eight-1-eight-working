'use client'
import React from 'react'
import { useSetState } from 'ahooks'
import Image from 'next/image'
interface arraySvg {
    arrSvg: {
        imageLink: string
        altName: string
        heightImage: number
        widthImage: number
    }[]
}

const MOBILEPAGE = () => {
    const [state, setState] = useSetState<arraySvg>({
        arrSvg: [
            {
                imageLink: require('@/assets/images/icons8-react-native.svg'),
                altName: 'react-native',
                heightImage: 100,
                widthImage: 100,
            },
            {
                imageLink: require('@/assets/images/icons8-javascript.svg'),
                altName: 'javascript',
                heightImage: 100,
                widthImage: 100,
            },
            {
                imageLink: require('@/assets/images/icons8-nodejs.svg'),
                altName: 'nodejs',
                heightImage: 100,
                widthImage: 100,
            },
            {
                imageLink: require('@/assets/images/icons8-mongodb.svg'),
                altName: 'mongodb',
                heightImage: 100,
                widthImage: 100,
            },
            {
                imageLink: require('@/assets/images/icons8-firebase.svg'),
                altName: 'firebase',
                heightImage: 100,
                widthImage: 100,
            },
            {
                imageLink: require('@/assets/images/icons8-express-js.svg'),
                altName: 'express-js',
                heightImage: 100,
                widthImage: 100,
            },
            {
                imageLink: require('@/assets/images/icons8-expo.svg'),
                altName: 'expo',
                heightImage: 100,
                widthImage: 100,
            },
            {
                imageLink: require('@/assets/images/icons8-redux.svg'),
                altName: 'redux',
                heightImage: 100,
                widthImage: 100,
            },
            {
                imageLink: require('@/assets/images/icons8-material-ui.svg'),
                altName: 'material-ui',
                heightImage: 100,
                widthImage: 100,
            },
            {
                imageLink: require('@/assets/images/icons8-bootstrap.svg'),
                altName: 'bootstrap',
                heightImage: 100,
                widthImage: 100,
            },
            {
                imageLink: require('@/assets/images/icons8-wordpress.svg'),
                altName: 'wordpress',
                heightImage: 100,
                widthImage: 100,
            },
            {
                imageLink: require('@/assets/images/icons8-tailwindcss.svg'),
                altName: 'tailwindcss',
                heightImage: 100,
                widthImage: 100,
            },
            {
                imageLink: require('@/assets/images/icons8-css.svg'),
                altName: 'css',
                heightImage: 100,
                widthImage: 100,
            },
            {
                imageLink: require('@/assets/images/materialize.svg'),
                altName: 'css',
                heightImage: 100,
                widthImage: 100,
            },
            {
                imageLink: require('@/assets/images/icons8-html.svg'),
                altName: 'html',
                heightImage: 100,
                widthImage: 100,
            },
            {
                imageLink: require('@/assets/images/icons8-nextjs.svg'),
                altName: 'nextjs',
                heightImage: 100,
                widthImage: 100,
            },
            {
                imageLink: require('@/assets/images/icons8-react.svg'),
                altName: 'react',
                heightImage: 100,
                widthImage: 100,
            },
            {
                imageLink: require('@/assets/images/pwa-svgrepo-com.svg'),
                altName: 'pwa-svgrepo-com',
                heightImage: 100,
                widthImage: 100,
            },
        ],
    })

    return (
        <div className="container">
            <h1 className="mt-4">Services</h1>
            <h5 className="mb-4">
                We specialize in providing cutting-edge point-of-sale solutions
                that enhance efficiency, customer experiences, and business
                growth. With a passionate team of POS experts, we empower
                businesses to thrive in the digital age of commerce.
            </h5>

            <section id="services" className="services">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-4 col-md-6">
                            <div className="icon-box" data-aos="fade-up">
                                <div className="icon">
                                    <i className="bi bi-laptop"></i>
                                </div>

                                <h4 className="title">
                                    <a href="">Custom POS System Development</a>
                                </h4>
                                <p className="description">
                                    We design and develop tailored POS systems
                                    that cater to your unique business needs.
                                    Whether you run a retail store, restaurant,
                                    or any other establishment, we create POS
                                    solutions that optimize operations and boost
                                    productivity.
                                </p>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6">
                            <div
                                className="icon-box"
                                data-aos="fade-up"
                                data-aos-delay="100"
                            >
                                <div className="icon">
                                    <Image
                                        alt=""
                                        src={require('@/assets/images/icons8-cloud-connection-100.png')}
                                        width={30}
                                        height={30}
                                    />
                                </div>
                                <h4 className="title">
                                    <a href="">Cloud-Based POS Systems</a>
                                </h4>
                                <p className="description">
                                    Our cloud-based POS solutions enable you to
                                    access your data and manage your business
                                    from anywhere, making remote management and
                                    scalability a breeze.
                                </p>
                            </div>
                        </div>

                        <div className="col-lg-4 col-md-6">
                            <div
                                className="icon-box"
                                data-aos="fade-up"
                                data-aos-delay="200"
                            >
                                <div className="icon">
                                    <Image
                                        alt=""
                                        src={require('@/assets/images/icons8-payment-method-64.png')}
                                        width={30}
                                        height={30}
                                    />
                                </div>
                                <h4 className="title">
                                    <a href="">Mobile POS (mPOS) Solutions</a>
                                </h4>
                                <p className="description">
                                    Extend your business&apos;s reach with
                                    mobile POS solutions. Our mPOS systems allow
                                    your staff to accept payments and serve
                                    customers on the go, improving customer
                                    service and reducing wait times.
                                </p>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6">
                            <div
                                className="icon-box"
                                data-aos="fade-up"
                                data-aos-delay="300"
                            >
                                <div className="icon">
                                    <Image
                                        alt=""
                                        src={require('@/assets/images/icons8-inventory-64.png')}
                                        width={30}
                                        height={30}
                                    />
                                </div>
                                <h4 className="title">
                                    <a href="">Inventory Management</a>
                                </h4>
                                <p className="description">
                                    Keep track of stock levels, automate
                                    reordering, and gain real-time insights into
                                    your inventory with our comprehensive
                                    inventory management features.
                                </p>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6">
                            <div
                                className="icon-box"
                                data-aos="fade-up"
                                data-aos-delay="300"
                            >
                                <div className="icon">
                                    <Image
                                        alt=""
                                        src={require('@/assets/images/icons8-combo-chart-100.png')}
                                        width={30}
                                        height={30}
                                    />
                                </div>
                                <h4 className="title">
                                    <a href="">Analytics and Reporting</a>
                                </h4>
                                <p className="description">
                                    Leverage data-driven insights to make
                                    informed business decisions. Our analytics
                                    and reporting tools help you monitor sales,
                                    track performance, and identify growth
                                    opportunities.
                                </p>
                            </div>
                        </div>
                        <div className="col-lg-4 col-md-6">
                            <div
                                className="icon-box"
                                data-aos="fade-up"
                                data-aos-delay="300"
                            >
                                <div className="icon">
                                    {/* <i className="bi bi-list-check"></i> */}
                                    <Image
                                        alt=""
                                        src={require('@/assets/images/icons8-support-100.png')}
                                        width={30}
                                        height={30}
                                    />
                                </div>
                                <h4 className="title">
                                    <a href="">24/7 Support and Maintenance</a>
                                </h4>
                                <p className="description">
                                    Our dedicated support team is available
                                    around the clock to assist with technical
                                    issues and provide ongoing maintenance to
                                    keep your POS system running smoothly.
                                </p>
                            </div>
                        </div>
                        <h1 className="mt-4">
                            Our <strong>Expertise</strong>
                        </h1>

                        <div className="row">
                            {state.arrSvg.map((e, i) => {
                                return (
                                    <div className="col-lg-3 col-md-6" key={i}>
                                        <div
                                            className="icon-box-1"
                                            data-aos="fade-up"
                                            data-aos-delay="300"
                                        >
                                            <Image
                                                src={e.imageLink}
                                                alt={e.altName}
                                                width={e.heightImage}
                                                height={e.widthImage}
                                            />
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                        <h5 className="mb-4">
                            Ready to modernize your business operations and
                            elevate customer experiences with our POS solutions?
                            Contact us today to discuss your project, request a
                            free consultation, and embark on a journey toward
                            enhanced efficiency and growth.
                        </h5>
                        <h5 className="mb-4">
                            we are committed to simplifying your business
                            processes and driving success through advanced POS
                            technology. Let&apos;s transform your business into a
                            more efficient and customer-friendly operation
                            together!
                        </h5>
                        {/* </div> */}
                    </div>
                </div>
            </section>
        </div>
    )
}

export default MOBILEPAGE
