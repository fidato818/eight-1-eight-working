"use client"
import React from 'react'
import { useSetState } from 'ahooks'
import Image from 'next/image'
interface arraySvg {
    arrSvg: { imageLink: string, altName: string, heightImage: number, widthImage: number }[]
}

const MOBILEPAGE = () => {
    const [state, setState] = useSetState<arraySvg>(
        {
            arrSvg: [
                { imageLink: require('@/assets/images/icons8-react-native.svg'), altName: "react-native", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-javascript.svg'), altName: "javascript", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-nodejs.svg'), altName: "nodejs", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-mongodb.svg'), altName: "mongodb", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-firebase.svg'), altName: "firebase", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-express-js.svg'), altName: "express-js", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-expo.svg'), altName: "expo", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-redux.svg'), altName: "redux", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-material-ui.svg'), altName: "material-ui", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-bootstrap.svg'), altName: "bootstrap", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-wordpress.svg'), altName: "wordpress", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-tailwindcss.svg'), altName: "tailwindcss", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-css.svg'), altName: "css", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/materialize.svg'), altName: "css", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-html.svg'), altName: "html", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-nextjs.svg'), altName: "nextjs", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/icons8-react.svg'), altName: "react", heightImage: 100, widthImage: 100 },
                { imageLink: require('@/assets/images/pwa-svgrepo-com.svg'), altName: "pwa-svgrepo-com", heightImage: 100, widthImage: 100 }

            ]
        }
    )

    return (
        <div className='container'>
            <h1 className='mt-4'>Services</h1>
            <h5 className='mb-4'>we are dedicated to revolutionizing your online retail presence. We are a leading e-commerce service provider specializing in creating, optimizing, and growing e-commerce businesses. With our extensive expertise, innovative solutions, and unwavering commitment to excellence, we empower businesses to thrive in the digital marketplace.</h5>

            <section id="services" className="services">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6 col-md-6">
                            <div className="icon-box" data-aos="fade-up">
                                <div className="icon">
                                    <i className="bi bi-laptop"></i>
                                </div>

                                <h4 className="title">
                                    <a href="">Custom E-commerce Solutions</a>
                                </h4>
                                <p className="description">
                                    If your business has unique requirements or complex workflows, we offer custom e-commerce development services. We design and develop bespoke solutions that cater to your specific needs.
                                </p>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-6">
                            <div
                                className="icon-box"
                                data-aos="fade-up"
                                data-aos-delay="100"
                            >
                                <div className="icon">
                                    <i className="bi bi-bar-chart-line-fill"></i>
                                </div>
                                <h4 className="title">
                                    <a href="">E-commerce Website Development</a>
                                </h4>
                                <p className="description">
                                    We build stunning, user-friendly e-commerce websites tailored to your brand. Our development team leverages cutting-edge technologies and platforms
                                </p>
                            </div>
                        </div>

                        <div className="col-lg-6 col-md-6">
                            <div
                                className="icon-box"
                                data-aos="fade-up"
                                data-aos-delay="200"
                            >
                                <div className="icon">
                                    <Image alt='' src={require('@/assets/images/icons8-multiple-devices-90.png')} width={30} height={30} />
                                </div>
                                <h4 className="title">
                                    <a href="">Responsive Design</a>
                                </h4>
                                <p className="description">
                                    Mobile commerce is on the rise. We create responsive and mobile-friendly e-commerce websites to ensure your customers can shop seamlessly on any device.
                                </p>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-6">
                            <div
                                className="icon-box"
                                data-aos="fade-up"
                                data-aos-delay="300"
                            >
                                <div className="icon">
                                    <i className="bi bi-list-check"></i>
                                </div>
                                <h4 className="title">
                                    <a href="">Security and Compliance</a>
                                </h4>
                                <p className="description">
                                    Protect your e-commerce store and customer data with robust security measures. We ensure your website complies with industry standards and regulations.
                                </p>
                            </div>
                        </div>
                        <h1 className='mt-4'>Our  <strong>Expertise</strong></h1>

                        <div className="row">
                            {state.arrSvg.map((e, i) => {
                                return (
                                    <div className="col-lg-3 col-md-6 mb-4" key={i}>
                                        <div
                                            className="icon-box-1"
                                            data-aos="fade-up"
                                            data-aos-delay="300">
                                            <Image
                                                src={e.imageLink}
                                                alt={e.altName}
                                                width={e.heightImage}
                                                height={e.widthImage} />
                                        </div>
                                    </div>
                                )
                            })}


                        </div>
                        <h1 className='mt-4 '>Why <strong>Choose us</strong></h1>

                        <div className="row pt-4">
                            <div className="col-lg-6 col-md-6">
                                <div className="icon-box" data-aos="fade-up">
                                    <div className="icon">
                                        <Image alt='' src={require('@/assets/images/icons8-e-commerce-100.png')} width={30} height={30} />
                                    </div>

                                    <h4 className="title">
                                        <a href="">E-commerce Expertise</a>
                                    </h4>
                                    <p className="description">
                                        With several years of experience in the e-commerce industry, we have a deep understanding of the unique challenges and opportunities it presents
                                    </p>
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6">
                                <div
                                    className="icon-box"
                                    data-aos="fade-up"
                                    data-aos-delay="100"
                                >
                                    <div className="icon">
                                        <i className="bi bi-people"></i>
                                    </div>
                                    <h4 className="title">
                                        <a href="">Client-Centric Approach</a>
                                    </h4>
                                    <p className="description">
                                        We prioritize your business goals and objectives, tailoring our solutions to align perfectly with your brand.
                                    </p>
                                </div>
                            </div>

                            <div className="col-lg-6 col-md-6">
                                <div
                                    className="icon-box"
                                    data-aos="fade-up"
                                    data-aos-delay="200"
                                >
                                    <div className="icon">
                                        <Image alt='' src={require('@/assets/images/icons8-result-64.png')} width={30} height={30} />


                                    </div>
                                    <h4 className="title">
                                        <a href="">Proven Results</a>
                                    </h4>
                                    <p className="description">
                                        Our portfolio showcases successful e-commerce projects across various industries.
                                    </p>
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-6">
                                <div
                                    className="icon-box"
                                    data-aos="fade-up"
                                    data-aos-delay="300"
                                >
                                    <div className="icon">
                                        <Image alt='' src={require('@/assets/images/icons8-scale-64.png')} width={40} height={40} />


                                    </div>
                                    <h4 className="title">
                                        <a href="">Scalability</a>
                                    </h4>
                                    <p className="description">
                                        We build e-commerce solutions that can grow with your business, ensuring flexibility and scalability for the long term.
                                    </p>
                                </div>
                            </div>
                            <h5 className='mb-4'>we are committed to transforming your online store into a thriving e-commerce powerhouse. Let&apos;s drive sales, enhance user experiences, and achieve remarkable growth together!</h5>
                        </div>
                        {/* </div> */}
                    </div>
                </div>
            </section>
        </div>
    )
}

export default MOBILEPAGE