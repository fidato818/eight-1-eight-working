import type { Metadata } from 'next'

// const db = getDatabase();
export const metadata: Metadata = {
    title: 'Services | 818 | Digital Agency',
    description:
        '818 | Digital Agency is a cutting-edge firm specializing in web design, digital marketing, and tech solutions. We propel businesses into the online world with creativity, data-driven strategies, and measurable results. Partner with us to thrive in the dynamic digital landscape and achieve your online objectives with confidence.',
}
export default function TestimonialLayout({
    children,
}: {
    children: React.ReactNode
}) {
    return <>{children}</>
}
