// 'use client'
import '../globals.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-icons/font/bootstrap-icons.css'
import 'aos/dist/aos.css'



import Navbar from '@/components/Navbar'
import Footer from '@/components/Footer'



type Props = {
    params: { id: string }
    searchParams?: { [key: string]: string | string[] | undefined }
    // searchParams?: { [key: string]: string | string[] | undefined | never }
}

var metaTitle = '818 | Digital Agency '
var metaDescription =
    '818 | Digital Agency is a cutting-edge firm specializing in web design, digital marketing, and tech solutions. We propel businesses into the online world with creativity, data-driven strategies, and measurable results. Partner with us to thrive in the dynamic digital landscape and achieve your online objectives with confidence.'
// export const metadata: Metadata = {
//     title: metaTitle,
//     description: metaDescription,
//     openGraph: {
//         title: metaTitle,
//         description: metaDescription,
//         url: 'https://nextjs.org',
//         siteName: '818 | Digital Agency',
//         locale: 'en_US',
//         type: 'website',
//     },
//     icons: {
//         icon: '/icon.png',
//         shortcut: '/shortcut-icon.png',
//         apple: '/apple-icon.png',
//         other: {
//             rel: 'apple-touch-icon-precomposed',
//             url: '/apple-touch-icon-precomposed.png',
//         },
//     },
//     manifest: 'https://nextjs.org/manifest.json',
// }

// export async function generateMetadata(
//     { params, searchParams }: Props,
//     // { params }: Props,
//     parent: ResolvingMetadata
// ): Promise<Metadata> {
//     const headersList = headers()
//     const pathname = headersList.get('pathname')
//     console.log('params', pathname)
//     return {
//         title: '...',
//     }
// }

const UserLayout = ({ children }: { children: React.ReactNode }) => {
    // useEffect(() => {
    //     AOS.init({
    //         duration: 1000,
    //         easing: 'ease-in-out',
    //         once: true,
    //         mirror: false,
    //     })
    // }, [])

    return (
        <>
            <Navbar />
            {children}
            <Footer />
        </>
    )
}
export default UserLayout
