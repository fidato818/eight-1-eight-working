'use client'
import type { Metadata } from 'next'
import { useSetState } from 'ahooks'
//Components
import ClientComponent from '@/components/client-section'
const metadata: Metadata = {
    title: '818 | Digital Agency | About',
    description:
        '818 | Digital Agency is a cutting-edge firm specializing in web design, digital marketing, and tech solutions. We propel businesses into the online world with creativity, data-driven strategies, and measurable results. Partner with us to thrive in the dynamic digital landscape and achieve your online objectives with confidence.',
}

const About = () => {
    const [state, setState] = useSetState({
        arrSkills: [
            { techName: 'HTML', progressWidth: '95%' },
            { techName: 'CSS', progressWidth: '80%' },
            { techName: 'JavaScript', progressWidth: '70%' },
            { techName: 'React', progressWidth: '80%' },
            { techName: 'React Native', progressWidth: '70%' },
            { techName: 'Expo', progressWidth: '60%' },
            { techName: 'MUI v5', progressWidth: '80%' },
            { techName: 'Tailwind CSS v3.0', progressWidth: '80%' },
            { techName: 'Bootstrap v5', progressWidth: '95%' },
            { techName: 'Materialize', progressWidth: '80%' },
            { techName: 'Next.js', progressWidth: '70%' },
            { techName: 'Redux-Toolkit', progressWidth: '70%' },
        ],
    })

    return (
        <div>
            <section id="about-us" className="about-us">
                <div className="container">
                    <div className="row no-gutters">
                        <div
                            className="image col-xl-5 d-flex align-items-stretch justify-content-center justify-content-lg-start"
                            data-aos="fade-right"
                        ></div>
                        <div className="col-xl-7 ps-0 ps-lg-5 pe-lg-1 d-flex align-items-stretch">
                            <div className="content d-flex flex-column justify-content-center">
                                <h3 data-aos="fade-up">About 818 | Digital Agency</h3>
                                <p data-aos="fade-up">
                                    <b> 818 | Digital Agency </b> is a
                                    versatile entity specializing in leveraging
                                    digital technologies to drive online
                                    success. It offer a range of services,
                                    including <b>Web Development</b>,{' '}
                                    <b>Mobile App Development</b>,{' '}
                                    <b>E-Commerce development</b> &{' '}
                                    <b>Progressive Web App</b> to help clients
                                    thrive in the ever-evolving online world.
                                    {/* A digital agency is a professional service firm specializing in creating, managing, and optimizing digital marketing and online presence strategies for businesses.  */}
                                </p>

                                <div className="row">
                                    <div
                                        className="col-md-6 icon-box"
                                        data-aos="fade-up"
                                    >
                                        <i className="bi bi-receipt"></i>

                                        <h4>
                                            Digital Strategy and Consultation
                                        </h4>
                                        <p>
                                            Digital agencies excel in developing
                                            strategic plans that align with a
                                            client&lsquo;s objectives.{' '}
                                        </p>
                                    </div>
                                    <div
                                        className="col-md-6 icon-box"
                                        data-aos="fade-up"
                                        data-aos-delay="100"
                                    >
                                        <i className="bi bi-window-stack"></i>
                                        <h4>Web Development and Design</h4>
                                        <p>
                                            A strong online presence is
                                            essential, and digital agencies
                                            offer expertise in designing and
                                            developing websites and
                                            applications.{' '}
                                        </p>
                                    </div>
                                    <div
                                        className="col-md-6 icon-box"
                                        data-aos="fade-up"
                                        data-aos-delay="200"
                                    >
                                        <i className="bi bi-phone"></i>
                                        <h4>Mobile App Development</h4>
                                        <p>
                                            A mobile app is a compact software
                                            designed for smartphones, offering
                                            specific functions or services,
                                            enhancing user experience
                                        </p>
                                    </div>
                                    <div
                                        className="col-md-6 icon-box"
                                        data-aos="fade-up"
                                        data-aos-delay="300"
                                    >
                                        <i className="bi bi-database-check"></i>
                                        <h4>Data-Driven Decision-Making</h4>
                                        <p>
                                            Digital agencies use data analytics
                                            to track campaign performance.
                                            Regular reporting ensures
                                            transparency and accountability
                                        </p>
                                    </div>
                                </div>

                                {/* <p data-aos="fade-up">
                                    If you like what we do, and think we could work together, then get in touch or launch our project planner.
                                </p> */}
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            {/* <section id="team" className="team section-bg">
                <div className="container">

                    <div className="section-title" data-aos="fade-up">
                        <h2>Our <strong>Team</strong></h2>
                        <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
                    </div>

                    <div className="row">

                        <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
                            <div className="member" data-aos="fade-up">
                                <div className="member-img">
                                    <img src="assets/img/team/team-1.jpg" className="img-fluid" alt="" />
                                    <div className="social">
                                        <a href=""><i className="bi bi-twitter"></i></a>
                                        <a href=""><i className="bi bi-facebook"></i></a>
                                        <a href=""><i className="bi bi-instagram"></i></a>
                                        <a href=""><i className="bi bi-linkedin"></i></a>
                                    </div>
                                </div>
                                <div className="member-info">
                                    <h4>Walter White</h4>
                                    <span>Chief Executive Officer</span>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
                            <div className="member" data-aos="fade-up" data-aos-delay="100">
                                <div className="member-img">
                                    <img src="assets/img/team/team-2.jpg" className="img-fluid" alt="" />
                                    <div className="social">
                                        <a href=""><i className="bi bi-twitter"></i></a>
                                        <a href=""><i className="bi bi-facebook"></i></a>
                                        <a href=""><i className="bi bi-instagram"></i></a>
                                        <a href=""><i className="bi bi-linkedin"></i></a>
                                    </div>
                                </div>
                                <div className="member-info">
                                    <h4>Sarah Jhonson</h4>
                                    <span>Product Manager</span>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
                            <div className="member" data-aos="fade-up" data-aos-delay="200">
                                <div className="member-img">
                                    <img src="assets/img/team/team-3.jpg" className="img-fluid" alt="" />
                                    <div className="social">
                                        <a href=""><i className="bi bi-twitter"></i></a>
                                        <a href=""><i className="bi bi-facebook"></i></a>
                                        <a href=""><i className="bi bi-instagram"></i></a>
                                        <a href=""><i className="bi bi-linkedin"></i></a>
                                    </div>
                                </div>
                                <div className="member-info">
                                    <h4>William Anderson</h4>
                                    <span>CTO</span>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-3 col-md-6 d-flex align-items-stretch">
                            <div className="member" data-aos="fade-up" data-aos-delay="300">
                                <div className="member-img">
                                    <img src="assets/img/team/team-4.jpg" className="img-fluid" alt="" />
                                    <div className="social">
                                        <a href=""><i className="bi bi-twitter"></i></a>
                                        <a href=""><i className="bi bi-facebook"></i></a>
                                        <a href=""><i className="bi bi-instagram"></i></a>
                                        <a href=""><i className="bi bi-linkedin"></i></a>
                                    </div>
                                </div>
                                <div className="member-info">
                                    <h4>Amanda Jepson</h4>
                                    <span>Accountant</span>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </section> */}
            {/* <!--End Our Team Section-- > */}

            {/* < !-- ======= Our Skills Section ======= --> */}
            <section id="skills" className="skills">
                <div className="container">
                    <div className="section-title" data-aos="fade-up">
                        <h2>
                            Our <strong>Skills</strong>
                        </h2>
                        <p>
                            Masters of  <u style={{ textUnderlineOffset: 4 }}>Next.js</u>,{' '}
                            <u style={{ textUnderlineOffset: 4 }}>
                                Bootstrap v5
                            </u>{' '}
                            , &  <u style={{ textUnderlineOffset: 4 }}>Materialize</u> for web excellence, we bring
                            efficiency with  <u style={{ textUnderlineOffset: 4 }}>Tailwind CSS v3.0</u>. Versatile
                            in  <u style={{ textUnderlineOffset: 4 }}>MUI v5</u>, we extend expertise to mobile apps
                            using  <u style={{ textUnderlineOffset: 4 }}>Expo</u> and  <u style={{ textUnderlineOffset: 4 }}>React Native</u>, ensuring
                            a seamless  <u style={{ textUnderlineOffset: 4 }}>React</u> ecosystem experience for
                            your dynamic and responsive development projects
                        </p>
                    </div>

                    <div className="row skills-content">
                        {state.arrSkills.map((e, i) => {
                            return (
                                <div
                                    className="col-lg-6"
                                    data-aos="fade-up"
                                    key={i}
                                >
                                    <div className="progress">
                                        <span className="skill">
                                            {e.techName}
                                            {/* <i className="val">{e.progressWidth}</i> */}
                                        </span>
                                        <div className="progress-bar-wrap">
                                            <div
                                                style={{
                                                    width: e.progressWidth,
                                                    transition: '0.9s',
                                                }}
                                                className="progress-bar"
                                                role="progressbar"
                                                aria-valuenow={100}
                                                aria-valuemin={0}
                                                aria-valuemax={100}
                                            ></div>
                                        </div>
                                    </div>
                                </div>
                            )
                        })}

                        {/* <div className="progress">
                                <span className="skill">WordPress/CMS <i className="val">90%</i></span>
                                <div className="progress-bar-wrap">
                                    <div className="progress-bar" role="progressbar" aria-valuenow={90} aria-valuemin={0} aria-valuemax={100}></div>

                                </div>
                            </div> */}
                    </div>
                </div>
            </section>

            <ClientComponent
                params={{
                    id: '',
                }}
            />
        </div>
    )
}

export default About
