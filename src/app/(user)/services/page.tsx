'use client'

import Image from 'next/image'
import ServiceComponent from '@/components/service-section'
import { useSetState } from 'ahooks'

const Services = () => {
    const [state, setState] = useSetState({
        changeTab: 1,
    })
    return (
        <main>
            <ServiceComponent />
            {/* <!-- End Services Section --> */}

            {/* <!-- ======= Features Section ======= --> */}
            <section id="features" className="features">
                <div className="container">
                    <div className="section-title" data-aos="fade-up">
                        <h2>
                            Some <strong>Features</strong> we do provide
                        </h2> 
                        <p>
                            Our agency is your one-stop solution for digital
                            success. We specialize in crafting bespoke mobile
                            and web apps, creating seamless e-commerce
                            experiences, and optimizing your retail operations
                            with efficient Point of Sale solutions. Elevate your
                            online presence and streamline business operations
                            with our comprehensive services.
                        </p>
                    </div>

                    <div className="row">
                        <div
                            className="col-lg-4 mb-5 mb-lg-0"
                            data-aos="fade-right"
                        >
                            <ul className="nav nav-tabs flex-column">
                                <li className="nav-item">
                                    <a
                                        style={{ cursor: 'pointer' }}
                                        className={
                                            state.changeTab === 1
                                                ? 'nav-link active show'
                                                : 'nav-link'
                                        }
                                        data-bs-toggle="tab"
                                        onClick={() =>
                                            setState({
                                                changeTab: 1,
                                            })
                                        }
                                    >
                                        <h4>Point of Sale</h4>
                                        <p>
                                            Provide top-notch Point of Sale
                                            (POS) solutions, streamlining sales,
                                            inventory management, and payment
                                            processing for your business
                                        </p>
                                    </a>
                                </li>
                                <li className="nav-item mt-2">
                                    <a
                                        style={{ cursor: 'pointer' }}
                                        className={
                                            state.changeTab === 2
                                                ? 'nav-link active show'
                                                : 'nav-link'
                                        }
                                        data-bs-toggle="tab"
                                        onClick={() =>
                                            setState({
                                                changeTab: 2,
                                            })
                                        }
                                    >
                                        <h4>Web Development</h4>
                                        <p>
                                            Web development involves designing,
                                            coding, and maintaining websites and
                                            web applications, creating online
                                            experiences and enhancing digital
                                            presence.
                                        </p>
                                    </a>
                                </li>
                                <li className="nav-item mt-2">
                                    <a
                                        style={{ cursor: 'pointer' }}
                                        className={
                                            state.changeTab === 3
                                                ? 'nav-link active show'
                                                : 'nav-link'
                                        }
                                        data-bs-toggle="tab"
                                        onClick={() =>
                                            setState({
                                                changeTab: 3,
                                            })
                                        }
                                    >
                                        <h4>E Commerce</h4>
                                        <p>
                                            Specializes in delivering
                                            comprehensive e-commerce solutions,
                                            from website development to payment
                                            integration, to drive your online
                                            business success.
                                        </p>
                                    </a>
                                </li>
                                <li className="nav-item mt-2">
                                    <a
                                        style={{ cursor: 'pointer' }}
                                        className={
                                            state.changeTab === 4
                                                ? 'nav-link active show'
                                                : 'nav-link'
                                        }
                                        data-bs-toggle="tab"
                                        onClick={() =>
                                            setState({
                                                changeTab: 4,
                                            })
                                        }
                                    >
                                        <h4>Mobile App Development</h4>
                                        <p>
                                            We offer full-stack mobile app
                                            development services, transforming
                                            your ideas into innovative,
                                            user-friendly applications for
                                            various platforms and industries.
                                        </p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div
                            className="col-lg-7 ml-auto"
                            data-aos="fade-left"
                            data-aos-delay="100"
                        >
                            <div className="tab-content">
                                <div
                                    className={
                                        state.changeTab === 1
                                            ? 'tab-pane active show'
                                            : 'tab-pane'
                                    }
                                    id="tab-1"
                                >
                                    <figure>
                                        <Image
                                            src={require('@/assets/images/features-1.png')}
                                            alt=""
                                            className="img-fluid"
                                        />
                                    </figure>
                                </div>
                                <div
                                    className={
                                        state.changeTab === 2
                                            ? 'tab-pane active show'
                                            : 'tab-pane'
                                    }
                                    id="tab-2"
                                >
                                    <figure>
                                        <Image
                                            src={require('@/assets/images/features-2.png')}
                                            alt=""
                                            className="img-fluid"
                                        />
                                    </figure>
                                </div>
                                <div
                                    className={
                                        state.changeTab === 3
                                            ? 'tab-pane active show'
                                            : 'tab-pane'
                                    }
                                    id="tab-3"
                                >
                                    <figure>
                                        <Image
                                            src={require('@/assets/images/features-3.png')}
                                            alt=""
                                            className="img-fluid"
                                        />
                                    </figure>
                                </div>
                                <div
                                    className={
                                        state.changeTab === 4
                                            ? 'tab-pane active show'
                                            : 'tab-pane'
                                    }
                                    id="tab-4"
                                >
                                    <figure>
                                        <Image
                                            src={require('@/assets/images/features-4.png')}
                                            alt=""
                                            className="img-fluid"
                                        />
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    )
}
export default Services
