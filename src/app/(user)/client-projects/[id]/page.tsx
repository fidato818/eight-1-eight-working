// import Ticket from '../client-projects'
// import axios from 'axios'
// let trendingTracks: any[] = []
// export async function generateStaticParams() {
//     try {
//         const response = await axios.get(
//             'https://adnan-ahmed.firebaseio.com/clientData.json?print=pretty',
//             { timeout: 1000 }
//         )

//         if (response.data) {
//             trendingTracks = []
//             for (var item of Object.values(response.data)) {
//                 trendingTracks.push(item)
//             }
//         }
//     } catch (error) {
//         console.error(error)
//     }

//     return trendingTracks.map((post: any) => ({
//         id: post.newPostKey.toString(),
//     }))
// }

// export default function Page({ params }: { params: { id: string } }) {
//     return <Ticket params={{ id: params.id }} />
// }

'use client'

import { useSetState } from 'ahooks'
import { ref, onValue } from 'firebase/database'
import { Parser } from 'html-to-react'
import Image from 'next/image'
import Link from 'next/link'
import moment from 'moment'

import { db } from '@/config/firebaseConfig'
import { useEffect } from 'react'

type Params = {
    id: string
}
const PortfolioDetail = ({ params }: { params: Params }) => {
    // console.log('params id', params)
    const [state, setState] = useSetState<any | null>({
        body: '',
        buttonLink: '',
        category: '',
        downloadURL: '',
        githubLink: '',
        headingName: '',
        updateAt: '',
        clientWebsite: '',
        createAt: '',
        arrTech: [],
        startDate: new Date(),
        endDate: new Date(),
    })

    useEffect(() => {
        const getData = async () => {
            const { id } = params

            // const response = await fetch(
            //     `https://adnan-ahmed.firebaseio.com/clientData.json?orderBy=\"newPostKey\"&equalTo=\"${id}\"`
            // )

            // const posts = await response.json()
            // console.log('response', response)
            // console.log('posts', posts)
            // const {
            //     body,
            //     buttonLink,
            //     category,
            //     githubLink,
            //     downloadURL,
            //     headingName,
            //     clientWebsite,
            //     createAt,
            //     arrTech,
            //     startDate,
            //     endDate,
            // } = Object.values(posts)[0] as any

            // setState({
            //     body,
            //     buttonLink,
            //     category,
            //     githubLink,
            //     downloadURL,
            //     headingName,
            //     clientWebsite,
            //     createAt,
            //     arrTech,
            //     startDate,
            //     endDate,
            // })
            // })

            const starCountRef = ref(db, 'clientData/' + id)
            onValue(starCountRef, (snapshot) => {
                var childData = snapshot.val()
                const {
                    body,
                    buttonLink,
                    category,
                    githubLink,
                    downloadURL,
                    headingName,
                    clientWebsite,
                    createAt,
                    arrTech,
                    startDate,
                    endDate,
                } = childData


                setState({
                    body,
                    buttonLink,
                    category,
                    githubLink,
                    downloadURL,
                    headingName,
                    clientWebsite,
                    createAt,
                    arrTech,
                    startDate,
                    endDate,
                })
            })
        }
        getData()
    }, [params, setState])

    const {
        body,
        category,
        downloadURL,
        headingName,
        clientWebsite,
        createAt,
        startDate,
        endDate,
    } = state
    const splitCategory = category.split('-')
    // console.log(splitCategory)
    return (
        <main>
            <section id="blog" className="blog">
                <div className="container" data-aos="fade-up">
                    <div className="row">
                        <div className="col-lg-8 entries">
                            <article className="entry entry-single">
                                <div className="entry-img">
                                    <Image
                                        src={downloadURL}
                                        alt=""
                                        className="img-fluid"
                                        width={100}
                                        height={100}
                                        style={{
                                            width: '100%',
                                            height: 'auto',
                                        }}
                                    />
                                </div>

                                <h2 className="entry-title">
                                    <Link href="#" className="text-capitalize">
                                        {headingName}
                                    </Link>
                                </h2>

                                <div className="entry-meta">
                                    <ul>
                                        {/* <li className="d-flex align-items-center"><i className="bi bi-person"></i> <a href="blog-single.html">John Doe</a></li>
                                        <li className="d-flex align-items-center"><i className="bi bi-clock"></i> <a href="blog-single.html">
                                            <time datetime="2020-01-01">Jan 1, 2020</time>
                                        </a></li> */}
                                        <li className="d-flex align-items-center">
                                            {/* <i className="bi bi-tags"></i> */}
                                            <span className="me-2">
                                                Category:{' '}
                                            </span>
                                            {/* <Link href="#"> */}
                                            <span
                                                style={{
                                                    textTransform: 'capitalize',
                                                    cursor: 'pointer',
                                                }}
                                            >
                                                {' '}
                                                {splitCategory[0]}{' '}
                                                {splitCategory[1]}{' '}
                                            </span>
                                            {/* </Link> */}
                                        </li>
                                    </ul>
                                </div>

                                <div className="entry-content">
                                    <p>{Parser().parse(body)}</p>

                                    {/* <p>
                                        Sit repellat hic cupiditate hic ut nemo. Quis nihil sunt non reiciendis. Sequi in accusamus harum vel aspernatur. Excepturi numquam nihil cumque odio. Et voluptate cupiditate.
                                    </p>

                                    <blockquote>
                                        <p>
                                            Et vero doloremque tempore voluptatem ratione vel aut. Deleniti sunt animi aut. Aut eos aliquam doloribus minus autem quos.
                                        </p>
                                    </blockquote>

                                    <p>
                                        Sed quo laboriosam qui architecto. Occaecati repellendus omnis dicta inventore tempore provident voluptas mollitia aliquid. Id repellendus quia. Asperiores nihil magni dicta est suscipit perspiciatis. Voluptate ex rerum assumenda dolores nihil quaerat.
                                        Dolor porro tempora et quibusdam voluptas. Beatae aut at ad qui tempore corrupti velit quisquam rerum. Omnis dolorum exercitationem harum qui qui blanditiis neque.
                                        Iusto autem itaque. Repudiandae hic quae aspernatur ea neque qui. Architecto voluptatem magni. Vel magnam quod et tempora deleniti error rerum nihil tempora.
                                    </p>

                                    <h3>Et quae iure vel ut odit alias.</h3>
                                    <p>
                                        Officiis animi maxime nulla quo et harum eum quis a. Sit hic in qui quos fugit ut rerum atque. Optio provident dolores atque voluptatem rem excepturi molestiae qui. Voluptatem laborum omnis ullam quibusdam perspiciatis nulla nostrum. Voluptatum est libero eum nesciunt aliquid qui.
                                        Quia et suscipit non sequi. Maxime sed odit. Beatae nesciunt nesciunt accusamus quia aut ratione aspernatur dolor. Sint harum eveniet dicta exercitationem minima. Exercitationem omnis asperiores natus aperiam dolor consequatur id ex sed. Quibusdam rerum dolores sint consequatur quidem ea.
                                        Beatae minima sunt libero soluta sapiente in rem assumenda. Et qui odit voluptatem. Cum quibusdam voluptatem voluptatem accusamus mollitia aut atque aut.
                                    </p>
                                    <img src="assets/img/blog/blog-inside-post.jpg" className="img-fluid" alt="" />

                                    <h3>Ut repellat blanditiis est dolore sunt dolorum quae.</h3>
                                    <p>
                                        Rerum ea est assumenda pariatur quasi et quam. Facilis nam porro amet nostrum. In assumenda quia quae a id praesentium. Quos deleniti libero sed occaecati aut porro autem. Consectetur sed excepturi sint non placeat quia repellat incidunt labore. Autem facilis hic dolorum dolores vel.
                                        Consectetur quasi id et optio praesentium aut asperiores eaque aut. Explicabo omnis quibusdam esse. Ex libero illum iusto totam et ut aut blanditiis. Veritatis numquam ut illum ut a quam vitae.
                                    </p>
                                    <p>
                                        Alias quia non aliquid. Eos et ea velit. Voluptatem maxime enim omnis ipsa voluptas incidunt. Nulla sit eaque mollitia nisi asperiores est veniam.
                                    </p> */}
                                </div>

                                {/* <div className="entry-footer">
                                    <i className="bi bi-folder"></i>
                                    <ul className="cats">
                                        <li><a href="#">Business</a></li>
                                    </ul>

                                    <i className="bi bi-tags"></i>
                                    <ul className="tags">
                                        <li><a href="#">Creative</a></li>
                                        <li><a href="#">Tips</a></li>
                                        <li><a href="#">Marketing</a></li>
                                    </ul>
                                </div> */}
                            </article>
                            {/* <!-- End blog entry --> */}

                            {/* <div className="blog-author d-flex align-items-center">
                                <img src="assets/img/blog/blog-author.jpg" className="rounded-circle float-left" alt="" />
                                <div>
                                    <h4>Jane Smith</h4>
                                    <div className="social-links">
                                        <a href="https://twitters.com/#"><i className="bi bi-twitter"></i></a>
                                        <a href="https://facebook.com/#"><i className="bi bi-facebook"></i></a>
                                        <a href="https://instagram.com/#"><i className="biu bi-instagram"></i></a>
                                    </div>
                                    <p>
                                        Itaque quidem optio quia voluptatibus dolorem dolor. Modi eum sed possimus accusantium. Quas repellat voluptatem officia numquam sint aspernatur voluptas. Esse et accusantium ut unde voluptas.
                                    </p>
                                </div>
                            </div> */}
                        </div>
                        {/* <!-- End blog entries list --> */}

                        <div className="col-lg-4">
                            <div className="sidebar">
                                <p className="mb-4" style={{ fontSize: 31 }}>
                                    Project information
                                </p>
                                <h3 className="sidebar-title">Client</h3>
                                <p className="text-capitalize">{headingName}</p>
                                <h3 className="sidebar-title mt-4">
                                    Scope of work
                                </h3>
                                <p className="text-uppercase">
                                    {state.category.replaceAll('-', ' ')}
                                </p>
                                {/* <>
                                    {' '}
                                    {console.log(
                                        'category',
                                        category.replaceAll('-', ' ')
                                    )}
                                </> */}
                                <h3 className="sidebar-title mt-4">
                                    Project date
                                </h3>
                                <p>
                                    {moment(startDate).format('MMM-YYYY')} -{' '}
                                    {moment(endDate).format('MMM-YYYY')}
                                </p>

                                <h3 className="sidebar-title mt-4 ">
                                    Project Link
                                </h3>
                                <Link
                                    href={`https://${clientWebsite}`}
                                    target="_blank"
                                    // type="button"
                                    className="btn btn-light btn-sm d-flex align-items-center  mb-2"
                                >
                                    <i className="bi bi-box-arrow-up-right me-1"></i>
                                    Check out
                                </Link>
                                {state.buttonLink !== '#' ? (
                                    <Link
                                        href={`${state.buttonLink}`}
                                        target="_blank"
                                        // type="button"
                                        className="btn btn-light btn-sm d-flex align-items-center"
                                    >
                                        <i className="bi bi-box-arrow-up-right me-1"></i>
                                        Check out - Mobile
                                    </Link>
                                ) : (
                                    ''
                                )}

                                {/* <!-- End sidebar categories--> */}
                                {/* <!-- End sidebar recent posts--> */}
                                <h3 className="sidebar-title mt-4 ">
                                    Technologies Used
                                </h3>
                                <div className="sidebar-item tags">
                                    <div className="d-flex flex-wrap">
                                        {state.arrTech &&
                                            state.arrTech.map(
                                                (e: any, i: number) => {
                                                    return (
                                                        <h4
                                                            style={{
                                                                cursor: 'pointer',
                                                            }}
                                                            key={i}
                                                        >
                                                            <span
                                                                style={{
                                                                    border: '1px solid #7666df',
                                                                    color: '#7666df',
                                                                }}
                                                                className="badge me-1 rounded-pill"
                                                            >
                                                                {e}
                                                            </span>
                                                        </h4>
                                                    )
                                                }
                                            )}

                                        {/* <h4 style={{ cursor: 'pointer' }}>
                                            <span
                                                style={{
                                                    border: '1px solid #7666df',
                                                    color: '#7666df',
                                                }}
                                                className="badge me-1 rounded-pill"
                                            >
                                                React Native
                                            </span>
                                        </h4>
                                        <h4 style={{ cursor: 'pointer' }}>
                                            <span
                                                style={{
                                                    border: '1px solid #7666df',
                                                    color: '#7666df',
                                                }}
                                                className="badge me-1 rounded-pill"
                                            >
                                                React Native Paper
                                            </span>
                                        </h4>
                                        <h4 style={{ cursor: 'pointer' }}>
                                            <span
                                                style={{
                                                    border: '1px solid #7666df',
                                                    color: '#7666df',
                                                }}
                                                className="badge me-1 rounded-pill"
                                            >
                                                Quiz Api for Quiz
                                            </span>
                                        </h4> */}
                                    </div>

                                    {/* <ul>
                                        <li><a href="#">App</a></li>
                                        <li><a href="#">IT</a></li>
                                        <li><a href="#">Business</a></li>
                                        <li><a href="#">Mac</a></li>
                                        <li><a href="#">Design</a></li>
                                        <li><a href="#">Office</a></li>
                                        <li><a href="#">Creative</a></li>
                                        <li><a href="#">Studio</a></li>
                                        <li><a href="#">Smart</a></li>
                                        <li><a href="#">Tips</a></li>
                                        <li><a href="#">Marketing</a></li>
                                    </ul> */}
                                </div>
                                {/* <!-- End sidebar tags--> */}
                            </div>
                            {/* <!-- End sidebar --> */}
                        </div>
                        {/* <!-- End blog sidebar --> */}
                    </div>
                </div>
            </section>
        </main>
    )
}
export default PortfolioDetail
