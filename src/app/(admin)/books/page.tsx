'use client'
import React from 'react'

import { useSetState } from 'ahooks'
import dynamic from 'next/dynamic'

const Page = () => {
    const Editor = dynamic(() => import('@/components/MyEditor'), {
        ssr: false,
    })
    const [state, setState] = useSetState({ value: '', body: '' })

    return (
        <>
            <div className="container">
                <Editor
                    value={'Foo'}
                    onChange={(v) => console.log(v)}
                    editorLoaded={false}
                    name={''}
                />
            </div>
        </>
    )
}
export default Page
