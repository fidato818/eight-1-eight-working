'use client'
import React, { useState, useEffect, useLayoutEffect } from 'react'
import {
    ref as ref_database,
    remove,
    onValue,
    child,
    push,
    set,
    update,
} from 'firebase/database'
import {
    getStorage,
    ref as ref_storage,
    deleteObject,
    uploadBytes,
    getDownloadURL,
} from 'firebase/storage'
import { db } from '@/config/firebaseConfig'
import Editor from '@/components/MyEditor'
import Image from 'next/image'
import {
    FormText,
    Input,
    ModalFooter,
    ModalBody,
    Modal,
    ModalHeader,
    Button,
} from 'reactstrap'
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap'

import { useSetState } from 'ahooks'
// import '../../Sidebar.css'
// import '../../screens/Admin/styles.css'
// import './style.module.css'
const storage = getStorage()

const Page = () => {
    const [state, setState] = useSetState<any | null>({
        headingName: '',
        category: 'web-apps',
        status: false,
        body: '',
        githubLink: '',
        buttonLink: '',
        arr: [],
        arrTech: [],
        image: null,
        url: '',
        progress: 0,
        currentPage: 0,
        pageSize: 5,
        toggleEditImage: false,
        visiableError: false,
        modalShow: false,
        editData: false,
        errorImage: false,
        modalDetail: false,
        loaderToggle: false,
        spinnerLoader: false,
        visible: false,
        errorMessage: '',
        downloadURL: '',
        newPostKey: '',
        createAt: '',
        updateAt: '',
        localImage: '',
        localImageKey: '',
        imageUpdate: '',
    })
    const [isClient, setIsClient] = useState(false)

    useEffect(() => {
        setIsClient(true)
    }, [])
    useLayoutEffect(() => {
        setState({ spinnerLoader: true })
        const getData = () => {
            const allPostData = ref_database(db, 'blogsData')
            onValue(allPostData, (snapshot) => {
                var newArr: any[] = []

                snapshot.forEach((data) => {
                    var childData = data.val()
                    newArr.push(childData)
                })

                setState({ arr: newArr.reverse(), spinnerLoader: false })
            })
        }
        getData()
    }, [setState])

    const customInputSwitched = (e: { target: { checked: any } }) => {
        setState({ status: e.target.checked })
    }
    const changeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        event.preventDefault()
        const { name, value } = event.target
        let obj: any = { [name]: value }
        setState(obj)
    }

    const handleClick = (
        e: React.MouseEvent<HTMLAnchorElement, MouseEvent>,
        index: number
    ) => {
        e.preventDefault()
        setState({
            currentPage: index,
        })
    }

    const handleChangeHandler = (e: any) => {
        if (e.target.files[0]) {
            const image = e.target.files[0]
            setState(() => ({ image }))
        }
    }

    const searchHandler = (e: { target: { value: any } }) => {
        const { arr } = state
        const textSearch = e.target.value
        // console.log(textSearch)
        const searchData = arr.filter((elem: { headingName: string }) => {
            return (
                elem.headingName
                    .substring(0, textSearch.length)
                    .toUpperCase() === textSearch.toUpperCase()
            )
        })

        setState({
            searchData,
            textSearch,
        })
    }

    const filterData = (e: number) => {
        setState({
            pageSize: e,
        })
    }

    const submitHandler = (e: { preventDefault: () => void }) => {
        e.preventDefault()
        const {
            status,
            headingName,
            body,
            githubLink,
            buttonLink,
            image,
            category,
            url,
            arrTech,
        } = state
        setState({
            loaderTog: true,
        })

        var newPostKey = push(child(ref_database(db), 'blogsData')).key
        const path = 'Avatar_' + newPostKey + `.${image.name.split('.').pop()}`

        const uploadTask = ref_storage(storage, `blogsData/${path}`)

        uploadBytes(uploadTask, image).then((snapshot) => {
            getDownloadURL(snapshot.ref).then((downloadURL) => {
                var obj = {
                    headingName,
                    body,
                    githubLink,
                    buttonLink,
                    image,
                    category,
                    status,
                    // currentUser,
                    createAt: new Date().toLocaleString(),
                    downloadURL,
                    newPostKey,
                    arrTech,
                }
                // console.log('path submitHandler', obj)
                set(ref_database(db, 'blogsData/' + newPostKey), obj)
                    .then((e) => {
                        setState({
                            visible: true,
                            modalShow: false,
                            loaderTog: false,
                        })
                    })
                    .catch((error) => {
                        console.log('Error: ', error)
                        setState({
                            errorMessage: error.message,
                            visiableError: true,
                        })
                    })
            })
        })
        // }
    }

    const deleteHandler = (id: any, image?: any) => {
        const singlePostData = ref_database(db, 'blogsData/' + id)
        remove(singlePostData)
            .then((e) => {
                console.log('success', e)
                alert('Deleted Successfully')

                let name = image.substr(
                    image.indexOf('%2F') + 3,
                    image.indexOf('?') - (image.indexOf('%2F') + 3)
                )
                name = name.replace('%20', ' ')
                const desertRef = ref_storage(storage, `blogsData/${name}`)

                deleteObject(desertRef)
                    .then(() => {
                        console.log('File deleted successfully')
                        // File deleted successfully
                    })
                    .catch((error) => {
                        // Uh-oh, an error occurred!
                    })
            })
            .catch((error) => {
                console.log('error', error)
                setState({
                    // errorMessage: error.message,
                    visiableError: true,
                })
            })
    }

    const handleUpdateImage = (e: any) => {
        if (e.target.files[0]) {
            const imageUpdate = e.target.files[0]
            setState(() => ({ imageUpdate }))
        }
    }
    const userImageUpdat = (e: { preventDefault: () => void }) => {
        e.preventDefault()
        setState({
            loaderToggle: true,
        })
        const { localImage, image, userDataKey, imageUpdate, localImageKey } =
            state

        // const path = 'Avatar_' + localImageKey + '.jpg'
        const path =
            'Avatar_' + localImageKey + `.${imageUpdate.name.split('.').pop()}`

        const uploadTask = ref_storage(storage, `blogsData/${path}`)

        uploadBytes(uploadTask, imageUpdate).then((snapshot) => {
            console.log('Uploaded a blob or file!')
            // Handle successful uploads on complete
            // For instance, get the download URL: https://firebasestorage.googleapis.com/...
            getDownloadURL(snapshot.ref).then((downloadURL) => {
                console.log('File available at', downloadURL)
                update(ref_database(db, 'blogsData/' + localImageKey), {
                    downloadURL,
                })
                    .then((e) => {
                        setState({
                            visible: true,
                            toggleEditImage: false,
                            loaderToggle: false,
                        })
                    })
                    .catch((error) => {
                        console.log('Error: ', error)
                        setState({
                            errorMessage: error.message,
                            visiableError: true,
                        })
                    })
            })
        })
    }

    const submitEdit = (e: { preventDefault: () => void }) => {
        e.preventDefault()
        const {
            status,
            headingName,
            category,
            body,
            githubLink,
            buttonLink,
            newPostKey,
            arrTech,
        } = state
        setState({
            loaderTog: true,
        })

        var obj = {
            headingName,
            category,
            body,
            githubLink,
            buttonLink,
            newPostKey,
            status,
            updateAt: new Date().toLocaleString(),
            arrTech,
        }
        update(ref_database(db, 'blogsData/' + newPostKey), obj)
            .then(() => {
                // props.history.push("/BlogData");
                console.log('success')
                // alert('success')
                setState({
                    // success: 'Successfully',
                    visible: true,
                    modalShow: false,
                    loaderTog: false,
                })
            })
            .catch((error) => {
                console.log('Error: ', error)
                alert('Error')
            })
    }
    const addTechHandler = () => {
        const { addTech, arrTech } = state

        setState((prevState) => ({
            // arrTech: [addTech, ...prevState.arrTech],
            arrTech: [...prevState.arrTech, addTech],
            addTech: '',
        }))
    }
    const removeTechHandler = (i: React.Key | null | undefined) => {
        const { arrTech } = state

        const removeTech = arrTech.filter(function (
            item: React.Key | null | undefined
        ) {
            return item !== i
        })
        setState((prevState) => ({
            arrTech: removeTech,
            // addTech: '',
        }))
    }

    const {
        arr,
        category,
        image,

        textSearch,
        searchData,
        currentPage,
        pageSize,
    } = state

    var imgCheck = image && image?.name
    const string = Math.ceil(arr.length / pageSize)
    var res = [...Array(string)]
    const sear = textSearch ? searchData : arr

    return (
        <>
            {isClient === true && (
                <div className="sb-nav-fixed " id="zoomed">
                    {/* <Navigations /> */}
                    <div id="layoutSidenav">
                        {/* <Sidebar /> */}
                        <div id="layoutSidenav_content">
                            <main>
                                <div className="container-fluid px-5">
                                    <div className="d-flex align-items-center justify-content-between ">
                                        <h1
                                            style={{
                                                color: 'rgb(118, 102, 223)',
                                                fontSize: 50,
                                            }}
                                            className="mt-3"
                                        >
                                            <i className="fas fa-tasks"></i>{' '}
                                            Blogs Section
                                        </h1>
                                        <button
                                            type="button"
                                            className="btn btn-outline-primary ml-auto"
                                            // data-toggle="modal"
                                            // data-target="#staticBackdrop "
                                            onClick={() =>
                                                setState({
                                                    modalShow: true,
                                                    headingName: '',
                                                    category: 'web-apps',
                                                    body: '',
                                                    githubLink: '',
                                                    buttonLink: '',
                                                    newPostKey: '',
                                                    downloadURL: '',
                                                    status: false,
                                                    editData: false,
                                                })
                                            }
                                        >
                                            Add Data
                                        </button>
                                    </div>

                                    {/* <div className="view-content">{renderPosts()}</div> */}
                                    <div>
                                        <div className="tile">
                                            <div
                                                style={{
                                                    display: 'flex',
                                                    justifyContent:
                                                        'space-between',
                                                }}
                                            >
                                                <div
                                                    style={{
                                                        marginBottom: 10,
                                                    }}
                                                >
                                                    <ul className=" nav nav-pills">
                                                        <li className="nav-item dropdown">
                                                            <a
                                                                className="nav-link dropdown-toggle active"
                                                                data-toggle="dropdown"
                                                                href="#/"
                                                                role="button"
                                                                aria-haspopup="true"
                                                                aria-expanded="false"
                                                            >
                                                                Show Enteries:{' '}
                                                                {pageSize}
                                                            </a>
                                                            <div className="dropdown-menu">
                                                                <a
                                                                    style={{
                                                                        cursor: 'pointer',
                                                                    }}
                                                                    className="dropdown-item"
                                                                    onClick={() =>
                                                                        filterData(
                                                                            5
                                                                        )
                                                                    }
                                                                >
                                                                    5
                                                                </a>
                                                                <a
                                                                    style={{
                                                                        cursor: 'pointer',
                                                                    }}
                                                                    className="dropdown-item"
                                                                    onClick={() =>
                                                                        filterData(
                                                                            10
                                                                        )
                                                                    }
                                                                >
                                                                    10
                                                                </a>
                                                                <a
                                                                    style={{
                                                                        cursor: 'pointer',
                                                                    }}
                                                                    className="dropdown-item"
                                                                    onClick={() =>
                                                                        filterData(
                                                                            25
                                                                        )
                                                                    }
                                                                >
                                                                    25
                                                                </a>
                                                                <a
                                                                    style={{
                                                                        cursor: 'pointer',
                                                                    }}
                                                                    className="dropdown-item"
                                                                    onClick={() =>
                                                                        filterData(
                                                                            100
                                                                        )
                                                                    }
                                                                >
                                                                    100
                                                                </a>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>

                                                <div
                                                    style={{
                                                        marginBottom: 10,
                                                    }}
                                                >
                                                    <input
                                                        className="form-control"
                                                        placeholder="Search by File Name"
                                                        onChange={searchHandler}
                                                    />
                                                </div>
                                            </div>

                                            <div className="table-responsive">
                                                <table
                                                    className="table table-bordered table-hover table-striped"
                                                    // id="dataTable"
                                                    width="100%"
                                                    cellSpacing={0}
                                                >
                                                    <thead>
                                                        <tr>
                                                            <th
                                                                style={{
                                                                    fontWeight: 100,
                                                                }}
                                                            >
                                                                Image
                                                            </th>
                                                            <th
                                                                style={{
                                                                    fontWeight: 100,
                                                                }}
                                                            >
                                                                File Name
                                                            </th>
                                                            <th
                                                                style={{
                                                                    fontWeight: 100,
                                                                }}
                                                            >
                                                                Category
                                                            </th>
                                                            <th
                                                                style={{
                                                                    fontWeight: 100,
                                                                }}
                                                            >
                                                                Create At
                                                            </th>
                                                            {/* <th style={{ fontWeight: 100 }}>Update At</th> */}
                                                            <th
                                                                style={{
                                                                    fontWeight: 100,
                                                                }}
                                                            >
                                                                Is Active
                                                            </th>

                                                            <th
                                                                style={{
                                                                    fontWeight: 100,
                                                                }}
                                                            >
                                                                Actions
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {sear
                                                            .slice(
                                                                currentPage *
                                                                    pageSize,
                                                                (currentPage +
                                                                    1) *
                                                                    pageSize
                                                            )
                                                            .map(
                                                                (
                                                                    en: any,
                                                                    index:
                                                                        | React.Key
                                                                        | null
                                                                        | undefined
                                                                ) => {
                                                                    return (
                                                                        <tr
                                                                            key={
                                                                                index
                                                                            }
                                                                        >
                                                                            <td>
                                                                                <Image
                                                                                    onClick={() =>
                                                                                        setState(
                                                                                            {
                                                                                                toggleEditImage:
                                                                                                    true,
                                                                                                localImage:
                                                                                                    en.downloadURL,
                                                                                                localImageKey:
                                                                                                    en.newPostKey,
                                                                                            }
                                                                                        )
                                                                                    }
                                                                                    src={
                                                                                        en.downloadURL
                                                                                    }
                                                                                    width={
                                                                                        50
                                                                                    }
                                                                                    height={
                                                                                        33
                                                                                    }
                                                                                    className="img-fluid"
                                                                                    alt=""
                                                                                    style={{
                                                                                        height: '33px',
                                                                                        width: '50px',
                                                                                    }}
                                                                                />
                                                                            </td>
                                                                            <td>
                                                                                {
                                                                                    en.headingName
                                                                                }
                                                                            </td>
                                                                            <td>
                                                                                {
                                                                                    en.category
                                                                                }
                                                                            </td>
                                                                            <td>
                                                                                {
                                                                                    en.createAt
                                                                                }
                                                                            </td>
                                                                            {/* <td >{en.updateAt}</td> */}
                                                                            <td>
                                                                                {en.status ===
                                                                                true ? (
                                                                                    <span
                                                                                        style={{
                                                                                            color: 'green',
                                                                                        }}
                                                                                    >
                                                                                        True
                                                                                    </span>
                                                                                ) : (
                                                                                    <span
                                                                                        style={{
                                                                                            color: 'red',
                                                                                        }}
                                                                                    >
                                                                                        False
                                                                                    </span>
                                                                                )}
                                                                            </td>

                                                                            <td
                                                                                style={{}}
                                                                            >
                                                                                <button
                                                                                    style={{
                                                                                        marginRight: 5,
                                                                                    }}
                                                                                    className="btn btn-success btn-sm"
                                                                                    onClick={() => {
                                                                                        setState(
                                                                                            {
                                                                                                headingName:
                                                                                                    en.headingName,
                                                                                                category:
                                                                                                    en.category,
                                                                                                body: en.body,
                                                                                                githubLink:
                                                                                                    en.githubLink,
                                                                                                buttonLink:
                                                                                                    en.buttonLink,
                                                                                                newPostKey:
                                                                                                    en.newPostKey,
                                                                                                downloadURL:
                                                                                                    en.downloadURL,
                                                                                                status: en.status,
                                                                                                // arrTech:
                                                                                                //     en.arrTech ==
                                                                                                //     undefined
                                                                                                //         ? en.arrTech
                                                                                                //         : [],
                                                                                                modalShow:
                                                                                                    true,
                                                                                                editData:
                                                                                                    true,
                                                                                            }
                                                                                        )
                                                                                    }}
                                                                                >
                                                                                    {/* <i className="fa  fa-trash fa-2x"></i>{" "} */}
                                                                                    EDIT
                                                                                </button>
                                                                                <button
                                                                                    // style={{ color: "red" }}
                                                                                    className="btn btn-danger btn-sm"
                                                                                    onClick={() => {
                                                                                        if (
                                                                                            window.confirm(
                                                                                                'Are you sure to delete this?'
                                                                                            )
                                                                                        ) {
                                                                                            deleteHandler(
                                                                                                en.newPostKey,
                                                                                                en.downloadURL
                                                                                            )
                                                                                        }
                                                                                    }}
                                                                                >
                                                                                    {/* <i className="fa  fa-trash fa-2x"></i>{" "} */}
                                                                                    DELETE{' '}
                                                                                </button>
                                                                            </td>
                                                                        </tr>
                                                                    )
                                                                }
                                                            )}
                                                    </tbody>
                                                </table>

                                                <div
                                                    style={{
                                                        display: 'flex',
                                                        // justifyContent: "space-between",
                                                        justifyContent:
                                                            'center',
                                                    }}
                                                >
                                                    <div
                                                        style={{
                                                            justifyContent:
                                                                'flex-start',
                                                        }}
                                                    >
                                                        {/* <p>
                            Showing 1 to {sear.length} of {sear.length} entries
                          </p> */}
                                                    </div>
                                                    <div
                                                        style={{
                                                            justifyContent:
                                                                'flex-end',
                                                        }}
                                                    >
                                                        <Pagination aria-label="Page navigation example">
                                                            <PaginationItem
                                                                disabled={
                                                                    currentPage <=
                                                                    0
                                                                }
                                                            >
                                                                <PaginationLink
                                                                    onClick={(
                                                                        e
                                                                    ) =>
                                                                        handleClick(
                                                                            e,
                                                                            currentPage -
                                                                                1
                                                                        )
                                                                    }
                                                                    previous
                                                                    href="javascript:void(0)"
                                                                />
                                                            </PaginationItem>
                                                            {res.map(
                                                                (page, i) => (
                                                                    <PaginationItem
                                                                        active={
                                                                            i ===
                                                                            currentPage
                                                                        }
                                                                        key={i}
                                                                    >
                                                                        <PaginationLink
                                                                            onClick={(
                                                                                e
                                                                            ) =>
                                                                                handleClick(
                                                                                    e,
                                                                                    i
                                                                                )
                                                                            }
                                                                            href="javascript:void(0)"
                                                                        >
                                                                            {i +
                                                                                1}
                                                                        </PaginationLink>
                                                                    </PaginationItem>
                                                                )
                                                            )}
                                                            <PaginationItem
                                                                disabled={
                                                                    currentPage >=
                                                                    res.length -
                                                                        1
                                                                }
                                                            >
                                                                <PaginationLink
                                                                    onClick={(
                                                                        e
                                                                    ) =>
                                                                        handleClick(
                                                                            e,
                                                                            currentPage +
                                                                                1
                                                                        )
                                                                    }
                                                                    next
                                                                    href="javascript:void(0)"
                                                                />
                                                            </PaginationItem>
                                                        </Pagination>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <Modal
                                        isOpen={state.modalShow}
                                        backdrop="static"
                                        size="xl"
                                        scrollable={true}
                                    >
                                        <ModalHeader
                                            close={
                                                <button
                                                    className="btn "
                                                    onClick={() =>
                                                        setState({
                                                            modalShow: false,
                                                        })
                                                    }
                                                >
                                                    X
                                                </button>
                                            }
                                        >
                                            {state.editData === true
                                                ? 'Edit'
                                                : 'Add'}{' '}
                                            Client Data
                                        </ModalHeader>
                                        <form
                                            onSubmit={
                                                state.editData === true
                                                    ? submitEdit
                                                    : submitHandler
                                            }
                                            method="post"
                                        >
                                            <ModalBody
                                                style={{
                                                    overflowY: 'scroll',
                                                    maxHeight: 400,
                                                }}
                                            >
                                                <div className="row mb-3">
                                                    <div className="col-md-4">
                                                        <input
                                                            className="form-control"
                                                            required
                                                            type="text"
                                                            value={
                                                                state.headingName
                                                            }
                                                            name="headingName"
                                                            id="exampleEmail"
                                                            placeholder="User Name"
                                                            onChange={
                                                                changeHandler
                                                            }
                                                        />
                                                    </div>
                                                    <div className="col-md-4">
                                                        <Input
                                                            className="form-control"
                                                            type="select"
                                                            name="category"
                                                            id="exampleSelect"
                                                            value={category}
                                                            onChange={
                                                                changeHandler
                                                            }
                                                        >
                                                            <option value="web-apps">
                                                                WEB APPS
                                                            </option>
                                                            <option value="mobile-app">
                                                                MOBILE APPS
                                                            </option>
                                                            <option value="web-app-with-mobile-app">
                                                                WEB APPS WITH
                                                                MOBILE APPS
                                                            </option>
                                                        </Input>
                                                    </div>
                                                    <div className="col-md-4">
                                                        <input
                                                            className="form-control"
                                                            type="text"
                                                            value={
                                                                state.buttonLink
                                                            }
                                                            placeholder="Country Name"
                                                            id="exampleText"
                                                            name="buttonLink"
                                                            onChange={
                                                                changeHandler
                                                            }
                                                            required
                                                        />
                                                    </div>
                                                </div>
                                                <div>
                                                    <Editor
                                                        //  style={{ height: 200 }}
                                                        value={state.body}
                                                        onChange={(v) =>
                                                            // console.log('v', v)
                                                            setState({
                                                                body: v,
                                                            })
                                                        }
                                                        // onChange={changeHandler}
                                                        editorLoaded={false}
                                                        key="editor1"
                                                        name={''}
                                                    />
                                                </div>
                                                <div className="row mt-3">
                                                    <div
                                                        className={
                                                            state.editData ===
                                                            true
                                                                ? 'col-md-8'
                                                                : 'col-md-4'
                                                        }
                                                    >
                                                        <input
                                                            className="form-control"
                                                            type="text"
                                                            value={
                                                                state.githubLink
                                                            }
                                                            id="exampleEmail"
                                                            placeholder="Freelance Website Name"
                                                            name="githubLink"
                                                            onChange={
                                                                changeHandler
                                                            }
                                                            required
                                                        />
                                                    </div>
                                                    {state.editData === true ? (
                                                        ''
                                                    ) : (
                                                        <div className="col-md-4">
                                                            <input
                                                                className="form-control"
                                                                required
                                                                type="file"
                                                                id="exampleCustomFileBrowser"
                                                                name="customFile"
                                                                onChange={
                                                                    handleChangeHandler
                                                                }
                                                            />
                                                            {imgCheck ||
                                                            state.errorImage ? (
                                                                <FormText color="danger">
                                                                    {image &&
                                                                    image.name
                                                                        ? ' '
                                                                        : 'Please Add Image'}
                                                                </FormText>
                                                            ) : (
                                                                <FormText color="danger"></FormText>
                                                            )}
                                                        </div>
                                                    )}
                                                    <div className="col-md-4">
                                                        <div className="mb-3 form-check">
                                                            <input
                                                                checked={
                                                                    state.status ===
                                                                    false
                                                                        ? false
                                                                        : true
                                                                }
                                                                type="checkbox"
                                                                name="status"
                                                                onChange={
                                                                    customInputSwitched
                                                                }
                                                                className="form-check-input"
                                                                id="exampleCheck1"
                                                            />
                                                            <label
                                                                className="form-check-label pl-4"
                                                                htmlFor="exampleCheck1"
                                                            >
                                                                {state.status ===
                                                                false
                                                                    ? 'Not Active'
                                                                    : 'Active'}
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div className="row mt-3">
                                                        <div className="col-md-12">
                                                            <div className="row  align-items-center">
                                                                <div className="col-md-9">
                                                                    <div className="input-group col-8">
                                                                        <input
                                                                            className="form-control"
                                                                            type="text"
                                                                            value={
                                                                                state.addTech
                                                                            }
                                                                            placeholder="Technologies Used"
                                                                            id="exampleText"
                                                                            name="addTech"
                                                                            onChange={
                                                                                changeHandler
                                                                            }
                                                                            // required
                                                                        />
                                                                    </div>
                                                                </div>

                                                                <div className="col-md-3 d-grid gap-2">
                                                                    <button
                                                                        type="button"
                                                                        className="btn btn-primary "
                                                                        onClick={() =>
                                                                            addTechHandler()
                                                                        }
                                                                    >
                                                                        Add Tech
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="mt-3">
                                                            {state.arrTech !==
                                                                undefined &&
                                                                state.arrTech.map(
                                                                    (
                                                                        e: any,
                                                                        i:
                                                                            | React.Key
                                                                            | null
                                                                            | undefined
                                                                    ) => {
                                                                        return (
                                                                            <span
                                                                                className="badge text-bg-primary me-2 "
                                                                                // style={{
                                                                                //     width: 100,
                                                                                // }}
                                                                                key={
                                                                                    i
                                                                                }
                                                                            >
                                                                                {
                                                                                    e
                                                                                }{' '}
                                                                                <button
                                                                                    type="button"
                                                                                    className="badge text-bg-danger btn btn-primary"
                                                                                    onClick={() =>
                                                                                        removeTechHandler(
                                                                                            e
                                                                                        )
                                                                                    }
                                                                                >
                                                                                    x
                                                                                </button>
                                                                            </span>
                                                                        )
                                                                    }
                                                                )}
                                                        </div>
                                                    </div>
                                                </div>
                                            </ModalBody>
                                            <ModalFooter>
                                                {state.editData === true ? (
                                                    <button
                                                        type="submit"
                                                        className="btn btn-outline-success"
                                                    >
                                                        Edit Data
                                                    </button>
                                                ) : (
                                                    <button
                                                        type="submit"
                                                        className="btn btn-outline-success"
                                                        disabled={
                                                            state.loaderTog
                                                                ? true
                                                                : false
                                                        }
                                                    >
                                                        Add Data
                                                    </button>
                                                )}
                                                <button
                                                    type="button"
                                                    className="btn btn-outline-secondary"
                                                    onClick={() =>
                                                        setState({
                                                            modalShow: false,
                                                        })
                                                    }
                                                >
                                                    Cancel
                                                </button>
                                            </ModalFooter>
                                        </form>
                                    </Modal>

                                    <Modal
                                        size="lg"
                                        isOpen={state.toggleEditImage}
                                        // toggle={state.toggle}
                                        //   className={className}
                                        //   backdrop={backdrop}
                                        backdrop="static"
                                        //   keyboard={keyboard}
                                    >
                                        <div
                                            className="mt-4 mr-4 ml-4"
                                            style={{
                                                display: 'flex',
                                                flexDirection: 'row',
                                                justifyContent: 'space-between',
                                            }}
                                        >
                                            <h5
                                            //  toggle={state.toggle}
                                            >
                                                Edit User Image
                                            </h5>
                                        </div>
                                        <hr />
                                        <form
                                            onSubmit={userImageUpdat}
                                            method="post"
                                        >
                                            <ModalBody>
                                                <div
                                                    className="pic"
                                                    style={{
                                                        justifyContent:
                                                            'center',
                                                        display: 'flex',
                                                    }}
                                                >
                                                    <Image
                                                        width={600}
                                                        height={800}
                                                        onClick={() =>
                                                            setState({
                                                                toggleEditImage:
                                                                    !state.toggleEditImage,
                                                            })
                                                        }
                                                        src={state.localImage}
                                                        className="img-fluid"
                                                        alt=""
                                                        style={{
                                                            height: '400px',
                                                            width: '550px',
                                                        }}
                                                    />
                                                </div>
                                                <div
                                                    className="custom-file  mt-4"
                                                    style={{
                                                        justifyContent:
                                                            'center',
                                                        display: 'flex',
                                                    }}
                                                >
                                                    <input
                                                        className="form-control"
                                                        required
                                                        type="file"
                                                        id="exampleCustomFileBrowser"
                                                        name="customFile"
                                                        // label={
                                                        //     image &&
                                                        //     image?.name
                                                        // }
                                                        onChange={
                                                            handleUpdateImage
                                                        }
                                                    />
                                                    {imgCheck ||
                                                    state.errorImage ? (
                                                        <FormText color="danger">
                                                            {image &&
                                                            image?.name
                                                                ? ' '
                                                                : 'Please Add Image'}
                                                        </FormText>
                                                    ) : (
                                                        <FormText color="danger"></FormText>
                                                    )}
                                                </div>
                                            </ModalBody>
                                            <ModalFooter
                                                style={{
                                                    display: 'flex',
                                                    justifyContent:
                                                        'space-between',
                                                }}
                                            >
                                                <div>
                                                    <Button
                                                        disabled={
                                                            state.loaderToggle
                                                                ? true
                                                                : false
                                                        }
                                                        className="mr-2"
                                                        color="primary"
                                                        type="submit"
                                                        // onClick={
                                                        //     () =>
                                                        //         userImageUpdat()
                                                        //     // setState({
                                                        //     //   modal: !state.modal,
                                                        //     // })
                                                        // }
                                                    >
                                                        Edit
                                                    </Button>

                                                    <Button
                                                        color="secondary"
                                                        onClick={() =>
                                                            setState({
                                                                toggleEditImage:
                                                                    false,
                                                            })
                                                        }
                                                    >
                                                        Cancel
                                                    </Button>
                                                </div>
                                            </ModalFooter>
                                        </form>
                                    </Modal>
                                </div>
                            </main>
                        </div>
                    </div>
                </div>
            )}
        </>
    )
}

export default Page
