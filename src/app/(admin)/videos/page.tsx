'use client'
import { useLayoutEffect } from 'react'
import { db } from '@/config/firebaseConfig'
import {
    ref,
    onValue,
    remove,
    update,
    set,
    child,
    push,
} from 'firebase/database'

import { useSetState } from 'ahooks'

interface stateData {
    headingName: string
    category: string
    status: boolean
    body: string
    githubLink: string
    buttonLink: string
    arr: any[]
    url: string
    progress: number
    currentPage: number
    pageSize: number
    editData: boolean
    spinnerLoader: boolean
    loaderToggle: boolean
    modalShow: boolean
    videoName: string
    videoId: string
    linkId: string
}

import { Modal, Spinner } from 'react-bootstrap'
const VideoPage = () => {
    const [state, setState] = useSetState<stateData>({
        headingName: '',
        category: 'web-apps',
        status: false,
        body: '',
        githubLink: '',
        buttonLink: '',
        arr: [],
        url: '',
        progress: 0,
        currentPage: 0,
        pageSize: 5,
        editData: false,
        loaderToggle: false,
        modalShow: false,
        videoName: '',
        videoId: '',
        linkId: '',
        spinnerLoader: false,
    })

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        event.preventDefault()
        const { name, value } = event.target
        let obj: any = { [name]: value }
        setState(obj)
    }

    useLayoutEffect(() => {
        setState({ spinnerLoader: true })
        const getData = () => {
            const allPostData = ref(db, 'VideoLink')
            onValue(allPostData, (snapshot) => {
                var arr: any[] = []
                const data = snapshot.val()
                snapshot.forEach((data) => {
                    var childData = data.val()
                    arr.push(childData)
                    // console.log(';', arr)
                })
    
                setState({ arr: arr.reverse(), spinnerLoader: false })
            })
        }    
        getData()
    }, [setState])
  
    const handlesubmit = (e: { preventDefault: () => void }) => {
        e.preventDefault()
        const { videoName, videoId, editData } = state
        const linkId = push(child(ref(db), 'VideosLink')).key
        if (editData === true) {
            update(ref(db, 'VideoLink/' + state.linkId), {
                videoName,
                videoId,
                linkId: state.linkId,
                updateAt: new Date().toLocaleString(),
            })
                .then(() => {
                    console.log('Document update successfully!')
                    setState({ modalShow: false })
                })
                .catch((error) => {
                    console.log('Error writing document: ', `${error}`)
                })
        } else if (editData === false) {
            set(ref(db, 'VideoLink/' + linkId), {
                videoName,
                videoId,
                createAt: new Date().toLocaleString(),
                linkId,
            })
                .then(() => {
                    console.log('Document set successfully!')
                    setState({ modalShow: false })
                })
                .catch((error) => {
                    console.log('Error writing document: ', `${error}`)
                })
        }
    }

    const deleteItem = (id: string, image?: undefined) => {
        const singlePostData = ref(db, 'VideoLink/' + id)
        remove(singlePostData)
            .then(() => console.log('Deleted'))
            .catch((err) => {
                console.log('err', err)
            })
    }

    const { arr, modalShow, videoName, videoId, spinnerLoader } = state

    return (
        <div className=" " id="">
            {/* {spinnerLoader === true ? (
                <div
                    className="d-flex align-items-center justify-content-center"
                    style={{ height: '70vh' }}
                >
                    <Spinner animation="border" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </Spinner>
                </div>
            ) : ( */}

            <main>
                <div className="container">
                    <Modal
                        // isOpen={state.modal}
                        backdrop="static"
                        // size="md"
                        scrollable={true}
                        show={modalShow}
                        onHide={() =>
                            setState({
                                modalShow: !state.modalShow,
                            })
                        }
                    >
                        <Modal.Header>
                            {state.editData === true ? 'Edit' : 'Add'} Videos
                            Link
                        </Modal.Header>
                        <form id="myForm" onSubmit={handlesubmit}>
                            <Modal.Body>
                                <div className="mb-3">
                                    <label
                                        htmlFor="exampleInputEmail1"
                                        className="form-label"
                                    >
                                        Video Name
                                    </label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="exampleInputEmail1"
                                        aria-describedby="emailHelp"
                                        placeholder="Video Name"
                                        required
                                        name="videoName"
                                        value={videoName}
                                        onChange={handleChange}
                                    />
                                </div>
                                <div className="mb-3">
                                    <label
                                        htmlFor="exampleInputPassword1"
                                        className="form-label"
                                    >
                                        Video Id
                                    </label>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="exampleInputPassword1"
                                        placeholder="Video Id"
                                        required
                                        name="videoId"
                                        value={videoId}
                                        onChange={handleChange}
                                    />
                                </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <button
                                    className="btn btn-primary"
                                    type="submit"
                                    disabled={
                                        state.loaderToggle === false
                                            ? false
                                            : true
                                    }
                                >
                                    {state.loaderToggle === true ? (
                                        <>
                                            <span
                                                className="spinner-border spinner-border-sm"
                                                role="status"
                                                aria-hidden="true"
                                            ></span>
                                            Loading...
                                        </>
                                    ) : (
                                        <>
                                            {state.editData === true
                                                ? 'Edit'
                                                : 'Add'}{' '}
                                            Item
                                        </>
                                    )}
                                </button>
                                <button
                                    type="button"
                                    className="btn btn-outline-secondary"
                                    onClick={() =>
                                        setState({
                                            modalShow: !state.modalShow,
                                        })
                                    }
                                >
                                    Close
                                </button>
                            </Modal.Footer>
                        </form>
                    </Modal>

                    <div className="d-flex align-items-center justify-content-between">
                        <h1
                            style={{
                                color: 'rgb(118, 102, 223)',
                                fontSize: 50,
                            }}
                            className="mt-3"
                        >
                            <i className="fas fa-tasks"></i> Videos Link
                        </h1>
                        <button
                            type="button"
                            className="btn btn-outline-primary ml-auto"
                            // data-toggle="modal"
                            // data-target="#staticBackdrop "
                            onClick={() =>
                                setState({
                                    modalShow: true,
                                    videoName: '',
                                    videoId: '',
                                    editData: false,
                                })
                            }
                        >
                            Add Link
                        </button>
                    </div>

                    <div>
                        <div className="row">
                            {arr.map((e: any, i) => {
                                return (
                                    <div
                                        className="col-xs-12 col-md-4 mt-4 mb-4 col-sm-6"
                                        key={i}
                                    >
                                        <div className="card">
                                            <iframe
                                                className="card-img-top embed-responsive-item"
                                                src={`https://www.youtube.com/embed/${e.videoId}`}
                                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                allowFullScreen
                                                title="Embedded youtube"
                                            />
                                            <div
                                                style={{
                                                    flex: '1 1 auto',
                                                    minHeight: '1px',
                                                    margin: '8px',
                                                }}
                                            >
                                                <h5
                                                    className="card-text user-select-none"
                                                    title={e.videoName}
                                                >
                                                    {e.videoName &&
                                                        e.videoName.substring(
                                                            0,
                                                            35
                                                        )}
                                                    ...
                                                </h5>
                                                <p className="card-text">
                                                    Create: {e.createAt}
                                                </p>
                                                {e.updateAt ? (
                                                    <p className="card-text">
                                                        Update: {e.updateAt}
                                                    </p>
                                                ) : (
                                                    <p className="card-text" />
                                                )}
                                            </div>
                                            <div className="card-footer text-muted d-flex  justify-content-end">
                                                <button
                                                    type="button"
                                                    className=" btn btn-success btn-sm me-2 "
                                                    onClick={() =>
                                                        setState({
                                                            videoName:
                                                                e.videoName,
                                                            videoId: e.videoId,
                                                            linkId: e.linkId,
                                                            modalShow: true,
                                                            editData: true,
                                                        })
                                                    }
                                                >
                                                    Edit
                                                </button>
                                                <button
                                                    type="button"
                                                    className="btn btn-danger btn-sm"
                                                    onClick={() => {
                                                        if (
                                                            window.confirm(
                                                                'Are you sure want to Delete this Item?'
                                                            )
                                                        ) {
                                                            deleteItem(e.linkId)
                                                        }
                                                    }}
                                                >
                                                    Delete
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </main>

            {/* // )} */}
        </div>
    )
}

export default VideoPage
